package edu.columbia.sportsleagueandroid.shared;

import com.google.web.bindery.requestfactory.shared.ProxyForName;
import com.google.web.bindery.requestfactory.shared.ValueProxy;

@ProxyForName(value = "edu.columbia.sportsleagueandroid.server.Batsman", locator = "edu.columbia.sportsleagueandroid.server.BatsmanLocator")
public interface BatsmanProxy extends ValueProxy 
{

	String getBatsmanName();

	void setBatsmanName(String batsmanName);

	int getRunsScored();

	void setRunsScored(int runsScored);

	int getBallsPlayed();

	void setBallsPlayed(int ballsPlayed);

	double getStrikeRate();

	void setStrikeRate(double strikeRate);

	String getStatus();

	void setStatus(String status);

	int getFours();

	void setFours(int fours);

	int getSixes();

	void setSixes(int sixes);

}
