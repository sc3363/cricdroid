package edu.columbia.sportsleagueandroid;

import java.util.ArrayList;
import java.util.List;

import com.google.web.bindery.requestfactory.shared.Receiver;
import com.google.web.bindery.requestfactory.shared.ServerFailure;

import edu.columbia.sportsleagueandroid.client.MyRequestFactory;
import edu.columbia.sportsleagueandroid.shared.MatchInningProxy;
import android.app.ListActivity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Toast;

public class ResultDisplayActivity extends ListActivity 
{
	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
	}
	
	private void setScreenContent()
	{
		String extraString = getIntent().getExtras().getString("extra");
		String[] matchStarterData = extraString.split("[\\|]");
		
		int matchNumber = Integer.parseInt(matchStarterData[0].substring(matchStarterData[0].indexOf("#")+2, matchStarterData[0].length()-1));
		String[] residualData = matchStarterData[1].split("' \n");
		String[] residualMatchData = residualData[0].split("\n");
		final String matchDate = residualMatchData[0].trim();
		String teams[] = residualMatchData[1].split(" vs ");
		final String teamOne = teams[0].replace("'", "").replace(" ", "");
		final String teamTwo = teams[1];
		String stadiumResidual[] = residualMatchData[2].split(", ");
		final String stadiumName = stadiumResidual[0];
		String stadiumCity = stadiumResidual[1];
		
		setMatchDetails(matchNumber, teamOne, teamTwo, stadiumName, stadiumCity);
	}

	private void setMatchDetails(final int matchNumber, final String teamOne, final String teamTwo, final String stadiumName,final String stadiumCity)
	{
		//setContentView(R.layout.nestedlive_layout);
		final List<MatchInningProxy> matchInning = new ArrayList<MatchInningProxy>();
		// Use an AsyncTask to avoid blocking the UI thread
		new AsyncTask<Void, Void, List<MatchInningProxy>>() 
		{
			@Override
			protected List<MatchInningProxy> doInBackground(Void... arg0) 
			{
				MyRequestFactory requestFactory = Util.getRequestFactory(getBaseContext(), MyRequestFactory.class);
						
				requestFactory.sportsLeagueAndroidRequest().queryMatchInnings(matchNumber).fire(new Receiver<List<MatchInningProxy>>() 
				{
					@Override
					public void onFailure(ServerFailure error)
					{
						String message = "Failure: "+ error.getMessage();
					}

					@Override
					public void onSuccess(List<MatchInningProxy> result) 
					{
						matchInning.addAll(result);
					}
				});
				
				return matchInning;
			}

			@Override
			protected void onPostExecute(final List<MatchInningProxy> matchInning) 
			{
				String[] passString; 
				
				if(matchInning == null || matchInning.size() == 0)
				{
					passString = new String[4];
					String matchDetailsString = "Match# "+matchNumber+"\n"+
    						""+teamOne+" vs "+teamTwo+"\n"+
    						stadiumName+", "+stadiumCity+"\n";
					passString[0] = matchDetailsString;
					passString[1] = "";
					passString[2] = "";
					passString[3] = "Match called off";
				}
				else
				{
					passString = new String[4];
					for (MatchInningProxy mProxy : matchInning) 
					{
						StringBuilder sB = new StringBuilder();
						sB.append("1,");
						sB.append(mProxy.getInningNumber()+",");
						sB.append(mProxy.getBattingTeam()+",");
						sB.append(mProxy.getFieldingTeam()+",");
						sB.append(mProxy.getScore()+",");
						sB.append(mProxy.getWickets()+",");
						sB.append(mProxy.getOvers()+";");
					}

		    
				    String matchDetailsString = "Match# "+matchInning.get(0).getMatchNumber()+"\n"+
				    						""+matchInning.get(0).getBattingTeam()+" vs "+matchInning.get(0).getFieldingTeam()+"\n"+
				    						stadiumName+", "+stadiumCity+"\n";
				    
				    String inningOneScore = matchInning.get(0).getBattingTeam()+" "+matchInning.get(0).getScore()+"/"+
							 matchInning.get(0).getWickets()+"    Overs: "+matchInning.get(0).getOvers();
				   
				    
				    
				    passString[0] = matchDetailsString;
				    passString[1] = inningOneScore;
				    
				    if((matchInning.get(0).getOvers() == 50.0) || (matchInning.get(0).getWickets() >= 10))
				    {
				    	String inningTwoScore = matchInning.get(1).getBattingTeam()+" "+matchInning.get(1).getScore()+"/"+
				    			matchInning.get(1).getWickets()+"    Overs: "+matchInning.get(1).getOvers();
				    	
				    	String matchInformation = "";
				    	
				    	if(matchInning.get(1).getWickets() < 10 && matchInning.get(1).getOvers()!=50.0)
				    	{
				    		if(matchInning.get(1).getScore() < matchInning.get(1).getScore())
				    		{
				    			int overs = (int) matchInning.get(1).getOvers();
					    		int balls = (int) ((matchInning.get(1).getOvers() - overs*1.0)*10);
					    		overs *= 6;
					    		overs += balls;
					    		matchInformation = matchInning.get(1).getBattingTeam()+" needs "+
					    				(matchInning.get(0).getScore()+1-matchInning.get(1).getScore())+" more runs from "+(300-overs)+" balls to win";
				    		}
				    		else
				    		{
				    			matchInformation = matchInning.get(1).getBattingTeam()+" won the match by "+(10 - matchInning.get(1).getWickets())+" wickets";
				    		}				    		
				    	}
				    	else
				    	{
				    		if(matchInning.get(1).getScore() < matchInning.get(0).getScore())
				    		{
				    			matchInformation = matchInning.get(0).getBattingTeam()+" won the match by "+
				    					(matchInning.get(0).getScore()-matchInning.get(1).getScore())+" runs";
				    		}
				    		else
				    		{
				    			inningTwoScore += matchInning.get(1).getBattingTeam()+" won the match by "+
				    					(10 - matchInning.get(1).getWickets())+" wickets";
				    		}
				    	}
				    	passString[2] = inningTwoScore;
				    	passString[3] = matchInformation;
				    }
				    else
				    {
				    	passString[2] = "";
				    	passString[3] = "";
				    }
				}
				
			    
			    final ArrayAdapter<String> adapter = new ArrayAdapter<String>(getBaseContext(), R.layout.scorecard_listitem, passString);
			    
			    setListAdapter(adapter);
				adapter.notifyDataSetChanged();
				ListView lv = getListView();
				lv.setTextFilterEnabled(true);
				
				lv.setOnItemClickListener(new OnItemClickListener()
				{
					public void onItemClick(AdapterView<?> lv, View view,int position, long id)
					{			
						if(position == 0)
						{
							Toast.makeText(getApplicationContext(), (CharSequence) lv.getItemAtPosition(position), Toast.LENGTH_SHORT).show();
						}
						else if(position == 1)
						{
							if(lv.getItemAtPosition(position).toString().equals(""))
							{
								Toast.makeText(getApplicationContext(), (CharSequence) "Nothing to display", Toast.LENGTH_SHORT).show();
							}
							else
							{
								Toast.makeText(getApplicationContext(), (CharSequence) lv.getItemAtPosition(position), Toast.LENGTH_SHORT).show();
								Intent myIntent = new Intent(getParent(), FullResultScorecardActivity.class);
								myIntent.putExtra("match_data", lv.getItemAtPosition(0).toString());
								myIntent.putExtra("inning_data", lv.getItemAtPosition(position).toString());
								myIntent.putExtra("inningNumber", position);
								ResultsActivityGroup parentActivity = (ResultsActivityGroup)getParent();
								parentActivity.startChildActivity("ResultsActivityGroup", myIntent);
							}
						}
						else if(position == 2)
						{
							if(lv.getItemAtPosition(position).toString().equals(""))
							{
								Toast.makeText(getApplicationContext(), (CharSequence) "Nothing to display", Toast.LENGTH_SHORT).show();
							}
							else
							{
								Toast.makeText(getApplicationContext(), (CharSequence) lv.getItemAtPosition(position), Toast.LENGTH_SHORT).show();
								Intent myIntent = new Intent(getParent(), FullResultScorecardActivity.class);
								myIntent.putExtra("match_data", lv.getItemAtPosition(0).toString());
								myIntent.putExtra("inning_data", lv.getItemAtPosition(position).toString());
								myIntent.putExtra("inningNumber", position);
								ResultsActivityGroup parentActivity = (ResultsActivityGroup)getParent();
								parentActivity.startChildActivity("ResultsActivityGroup", myIntent);
							}
						}
						else if(position == 3)
						{
							if(lv.getItemAtPosition(position).toString().equals(""))
							{
								Toast.makeText(getApplicationContext(), (CharSequence) "Nothing to display", Toast.LENGTH_SHORT).show();
							}
							else
							{
								Toast.makeText(getApplicationContext(), (CharSequence) lv.getItemAtPosition(position), Toast.LENGTH_SHORT).show();
							}
						}
					}
				});
			}
		}.execute();
	}
	
	@Override
	public void onResume() 
	{
	//	onDestroy();
		super.onResume();
		
		setScreenContent();
	//	setContentView(R.layout.nestedlive_layout);
	}
	
	 @Override
	 public void onDestroy()
	 {
	     super.onDestroy();
	 }
	
	

	

}
