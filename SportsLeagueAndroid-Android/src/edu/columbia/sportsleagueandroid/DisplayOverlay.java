package edu.columbia.sportsleagueandroid;

/*
 * File: DisplayOverlay.java
 * Author: Prashant Bhardwaj (pb2466)
 * References: Android Developer Forum, Open-Source Development online resources
 */

import java.util.ArrayList;
import java.util.List;

import android.app.AlertDialog;
import android.content.Context;
import android.graphics.drawable.Drawable;

import com.google.android.maps.ItemizedOverlay;
import com.google.android.maps.OverlayItem;

public class DisplayOverlay extends ItemizedOverlay
{
	Context activityContext;
	private List<OverlayItem> mapOverlaysList = new ArrayList<OverlayItem>();

	//Display User-Pin
	public DisplayOverlay(Drawable pin) 
	{
		  super(boundCenterBottom(pin));
	}
	
	//Display overlay
	public DisplayOverlay(Drawable pin, Context context) 
	{
		  super(boundCenterBottom(pin));
		  activityContext = context;
	}

	//Populate overlay list
	public void addOverlay(OverlayItem mapOverlay) 
	{
		mapOverlaysList.add(mapOverlay);
	    populate();
	}

	//Create overlay item
	@Override
	protected OverlayItem createItem(int itemIndex) 
	{
	  return mapOverlaysList.get(itemIndex);
	}

	//Return overlay list size
	@Override
	public int size() 
	{
	  return mapOverlaysList.size();
	}
	
	//Set on tap functionality
	@Override
	protected boolean onTap(int itemIndex) 
	{
	  OverlayItem overlayItem = mapOverlaysList.get(itemIndex);
	  AlertDialog.Builder dialog = new AlertDialog.Builder(activityContext);
	  dialog.setTitle(overlayItem.getTitle());
	  dialog.setMessage(overlayItem.getSnippet());
	  dialog.show();
	  
	  return true;
	}

}
