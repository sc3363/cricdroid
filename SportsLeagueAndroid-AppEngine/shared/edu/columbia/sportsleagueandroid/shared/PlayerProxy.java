package edu.columbia.sportsleagueandroid.shared;

import com.google.web.bindery.requestfactory.shared.ProxyForName;
import com.google.web.bindery.requestfactory.shared.ValueProxy;

@ProxyForName(value = "edu.columbia.sportsleagueandroid.server.Player", locator = "edu.columbia.sportsleagueandroid.server.PlayerLocator")
public interface PlayerProxy extends ValueProxy {

	String getPlayerName();

	void setPlayerName(String playerName);

	String getPlayerTeam();

	void setPlayerTeam(String playerTeam);

	int getPlayerAge();

	void setPlayerAge(int playerAge);

	int getMatchesPlayed();

	void setMatchesPlayed(int matchesPlayed);

	int getRunsScored();

	void setRunsScored(int runsScored);

	int getWicketsTaken();

	void setWicketsTaken(int wicketsTaken);

}
