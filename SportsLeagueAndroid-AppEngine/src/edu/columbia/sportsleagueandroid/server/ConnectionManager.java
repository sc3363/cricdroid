package edu.columbia.sportsleagueandroid.server;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import com.google.appengine.api.rdbms.AppEngineDriver;

public class ConnectionManager 
{
	private static Connection dbConnection;
	private static ConnectionManager instance = null;

	protected ConnectionManager() 
	{
		// Exists only to defeat instantiation.
	}

	public static ConnectionManager getInstance() 
	{
		if (instance == null) 
		{
			instance = new ConnectionManager();
		}
		return instance;
	}

	public Connection getConnection() 
	{
		try 
		{
			if (dbConnection != null) 
			{
				if (!dbConnection.isClosed()) 
				{
					if( !(dbConnection.getTransactionIsolation()== Connection.TRANSACTION_NONE))
					{
						dbConnection = DriverManager.getConnection("jdbc:google:rdbms://jpsdb2012:transcloudinstance/sports_cricket_db");
					}
					else
					{
						return dbConnection;
					}
				}
			}
			else 
			{
				DriverManager.registerDriver(new AppEngineDriver());
				dbConnection = DriverManager.getConnection("jdbc:google:rdbms://jpsdb2012:transcloudinstance/sports_cricket_db");
			}

		}
		catch (SQLException sqle) 
		{
			try 
			{
				dbConnection.close();
			}
			catch (SQLException exc)
			{
				exc.printStackTrace();
			}
		}
		catch (Exception e)
		{
			try
			{
				dbConnection.close();
			}
			catch (SQLException exc)
			{
				exc.printStackTrace();
			}
		}

		return dbConnection;
	}

	public static Connection getFreshConnection() 
	{
		// TODO Auto-generated method stub
		try 
		{
			dbConnection = DriverManager.getConnection("jdbc:google:rdbms://jpsdb2012:transcloudinstance/sports_cricket_db");
		}
		catch (SQLException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return dbConnection;
	}
}
