package edu.columbia.sportsleagueandroid.server;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.SQLNonTransientException;
import java.sql.SQLRecoverableException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;


public class ScoreboardActions 
{
	private static String MATCH_INNINGS_TABLE = "match_inning";
	private static String PLAYER_TABLE = "player";
	private static String MATCH_OVER_TABLE = "match_over";
	private static String MATCH_SCHEDULE_TABLE = "match_schedule";
	private static String STADIUM_TABLE = "stadium";
	private static String TEAM_TABLE = "team";
	private static String SCORECARD_TABLE = "scorecard";

	public ScoreboardActions()
	{}
	
	
	/* ****************************************************** Update Innings During Ongoing Match ********************************************************* */
	public MatchInning UpdateInningDuringMatch(Connection dbConnection, MatchOver matchover) throws SQLException, IOException
	{
		int success = -1;

		MatchInning matchInning = new MatchInning();
		
		String updateStatement = "";
		String fetchStatement = "select batting_team,score,overs,wickets from "+ MATCH_INNINGS_TABLE + " where match_number="+matchover.getMatchNumber()
				+ " and inning_number="+ matchover.getInningNumber();
		
		double inningOvers = 0;
		int score = 0;
		int wickets = 0;
		String battingTeam = "";
		
		Statement fetchStmt = dbConnection.createStatement();
		ResultSet rs = fetchStmt.executeQuery(fetchStatement);

		while (rs.next()) 
		{
			battingTeam = rs.getString("batting_team");
			score = rs.getInt("score");
			inningOvers = rs.getDouble("overs");
			wickets = rs.getInt("wickets");
		}
		
		String oB = inningOvers+"";
		inningOvers = Double.parseDouble(oB.substring(0, oB.indexOf(".")+2));
		
		String inningOversString = inningOvers + "";

		//If the last bowl of the over has been bowled
		if (inningOversString.contains(".5")) 
		{
			if (!(matchover.getAction().equalsIgnoreCase("WD") || matchover.getAction().equalsIgnoreCase("NB"))) 
			{
				inningOvers = inningOvers+1	- Double.valueOf(inningOversString.substring(inningOversString.indexOf("."),inningOversString.length()));
			}
		}
		else
		{
			inningOvers = inningOvers + 0.1;
		}

		//If the ball produced a wicket
		if (matchover.getAction().equalsIgnoreCase("W")) 
		{
			updateStatement = "UPDATE " + MATCH_INNINGS_TABLE+ " SET score = score+" + matchover.getRunsScored()+ ", wickets=wickets+1, " + "overs=" + inningOvers
					+ " where match_number=" + matchover.getMatchNumber()+ " and inning_number=" + matchover.getInningNumber();
			wickets++;
		}
		else
		{
			updateStatement = "UPDATE " + MATCH_INNINGS_TABLE+ " SET score = score+" + matchover.getRunsScored()
					+ ", overs=" + inningOvers+ " where match_number="+matchover.getMatchNumber()+" and inning_number="+matchover.getInningNumber();
			score = score+matchover.getRunsScored();
		}

		PreparedStatement preparedStatement = dbConnection.prepareStatement(updateStatement);

		success = preparedStatement.executeUpdate();

		dbConnection.commit();
	
		matchInning.setBattingTeam(battingTeam);
		matchInning.setScore(score);
		matchInning.setOvers(inningOvers);
		matchInning.setWickets(wickets);
		
		return matchInning;
	}

	
	/* ****************************************************** Update Innings Before Match Start Time ********************************************************* */
	public int UpdateInningBeforeStartOfMatch(Connection dbConnection, MatchInning matchInning) throws SQLException, IOException
	{
		String updateStatement = "INSERT INTO " + MATCH_INNINGS_TABLE+ " VALUES (?,?,?,?,?,?,?)";
		int success = -1;

		PreparedStatement preparedStatement = dbConnection.prepareStatement(updateStatement);

		preparedStatement.setInt(1, matchInning.getMatchNumber());
		preparedStatement.setInt(2, matchInning.getInningNumber());
		preparedStatement.setString(3, "'"+matchInning.getBattingTeam()+"'");
		preparedStatement.setString(4, "'"+matchInning.getFieldingTeam()+"'");
		preparedStatement.setInt(5, matchInning.getScore());
		preparedStatement.setInt(6, matchInning.getWickets());
		preparedStatement.setDouble(7, matchInning.getOvers());

		success = preparedStatement.executeUpdate();

		dbConnection.commit();
		
		return success;
	}

	
	/* ****************************************************** Update Team Records ********************************************************* */
	public int UpdateTeams(Connection dbConnection, Team team) throws SQLException, IOException
	{
		String updateStatement = "INSERT INTO " + TEAM_TABLE+ " VALUES (?,?,?,?,?,?)";
		int success = -1;

		PreparedStatement preparedStatement = dbConnection.prepareStatement(updateStatement);

		preparedStatement.setString(1, "'"+team.getTeamName()+"'");
		preparedStatement.setInt(2, team.getTeamRanking());
		preparedStatement.setInt(3, team.getMatchCount());
		preparedStatement.setInt(4, team.getWins());
		preparedStatement.setInt(5, team.getLosses());
		preparedStatement.setInt(6, team.getNoResult());

		success = preparedStatement.executeUpdate();

		dbConnection.commit();
		
		return success;
	}

	
	/* ****************************************************** Fetch Master Scorecard for Ongoing Match ********************************************************* */
	public MasterScorecard FetchMasterScorecard(Connection dbConnection, final int matchNumber, final int inningNumber, final String battingTeam, 
			final String fieldingTeam, final int teamScore, final int teamWickets, final double teamOvers, final String stadiumName,final String stadiumCity) 
			throws SQLException, IOException 
	{
		MasterScorecard masterScorecard = new MasterScorecard();
		
		String fetchBattingScorecard = "select s.player_name,s.player_score,s.player_balls,s.player_status,s.player_fours,s.player_sixes" +
				" from scorecard s,match_inning m where m.match_number=s.match_number and " +
				"m.match_number="+matchNumber+" and m.inning_number="+inningNumber+" and s.player_team='"+
				battingTeam+"' order by s.player_balls desc";

		
		String fetchBowlingScorecard = "select distinct s.player_name,s.player_balls_bowled,s.player_runs_conceded,s.player_wickets" +
				" from scorecard s,match_inning m where m.match_number=s.match_number and s.match_number="+matchNumber+
				" and s.player_team=(select fielding_team from match_inning where match_number=s.match_number and inning_number="+inningNumber+
				") order by s.player_balls_bowled desc;";

		ResultSet rs;
		
		Statement battingScorecardFetch = dbConnection.createStatement();
		rs = battingScorecardFetch.executeQuery(fetchBattingScorecard);
		List<Batsman> batsmanList = new ArrayList<Batsman>();
		List<Bowler> bowlerList = new ArrayList<Bowler>();
			
		/* ---------------------------------------------------- Fetch Batsman Scorecard --------------------------------------------------------- */
		while(rs.next())
		{
			Batsman batsman = new Batsman();
			batsman.setBatsmanName(rs.getString("player_name"));
			batsman.setRunsScored(rs.getInt("player_score"));
			batsman.setBallsPlayed(rs.getInt("player_balls"));
			batsman.setFours(rs.getInt("player_fours"));
			batsman.setSixes(rs.getInt("player_sixes"));
			batsman.setStatus(rs.getString("player_status"));
			batsmanList.add(batsman);
		}
			
		Statement bowlingScorecardFetch = dbConnection.createStatement();
		rs = null;
		rs = bowlingScorecardFetch.executeQuery(fetchBowlingScorecard);
			
		/* ---------------------------------------------------- Fetch Bowler Scorecard --------------------------------------------------------- */
		while(rs.next())
		{
			Bowler bowler = new Bowler();
			bowler.setBowlerName(rs.getString("player_name"));
			int ballsBowled = rs.getInt("player_balls_bowled");
			int oversBowled = ballsBowled/6;
			int ballsRemainder = ballsBowled%6;
			double actualOversBowled = oversBowled + (ballsRemainder*0.1);
			String oB = actualOversBowled+"";
			double ov = Double.parseDouble(oB.substring(0, oB.indexOf(".")+2));
			bowler.setOversBowled(ov);
			bowler.setRunsConceded(rs.getInt("player_runs_conceded"));
			bowler.setBowlerWickets(rs.getInt("player_wickets"));
			bowlerList.add(bowler);
		}
			
		/* ---------------------------------------------------- Set up the Scorecard --------------------------------------------------------- */
		masterScorecard.setMatchNumber(matchNumber);
		masterScorecard.setScore(teamScore);
		masterScorecard.setOvers(teamOvers);
		masterScorecard.setWickets(teamWickets);
		masterScorecard.setTeamOne(battingTeam);
		masterScorecard.setTeamTwo(fieldingTeam);
		masterScorecard.setStadiumName(stadiumName);
		masterScorecard.setStadiumCity(stadiumCity);
		masterScorecard.setBatsman(batsmanList);
		masterScorecard.setBowler(bowlerList);
	
		return masterScorecard;
	}

	
	/* ****************************************************** Fetch Preview Scorecard for Ongoing Match ********************************************************* */
	public MasterScorecard FetchPreviewScorecard(Connection dbConnection, int matchNumber, int inningNumber) 
			throws SQLException, IOException 
	{
		MasterScorecard masterScorecard = new MasterScorecard();
		
		String fetchBattingPreview = "select s.match_number,m.inning_number,s.player_name,s.player_score,s.player_balls from scorecard s,match_inning m " +
				"where m.match_number=s.match_number and m.match_number="+matchNumber+" and m.inning_number=1" +
						" and s.player_team=m.batting_team and s.player_status = 'NO' order by s.player_score desc";
		
		String fetchBowlingPreview = "select distinct s.match_number,m.inning_number,s.player_name,s.player_balls_bowled,s.player_runs_conceded,s.player_wickets "+
				"from scorecard s,match_inning m where m.match_number=s.match_number and s.match_number="+matchNumber+" and m.inning_number="+inningNumber+
				" and s.player_team=(select fielding_team from match_inning where match_number="+matchNumber+" and "+inningNumber+"=inning_number) " +
						"and s.player_wickets>0 order by s.player_wickets desc";

		ResultSet rs;
		
		Statement battingScorecardFetch = dbConnection.createStatement();
		rs = battingScorecardFetch.executeQuery(fetchBattingPreview);
		List<Batsman> batsmanList = new ArrayList<Batsman>();
		List<Bowler> bowlerList = new ArrayList<Bowler>();
			
		/* ---------------------------------------------------- Fetch Batsman Scorecard --------------------------------------------------------- */
		while(rs.next())
		{
			Batsman batsman = new Batsman();
			batsman.setBatsmanName(rs.getString("player_name"));
			batsman.setRunsScored(rs.getInt("player_score"));
			batsman.setBallsPlayed(rs.getInt("player_balls"));
			batsmanList.add(batsman);
		}
			
		Statement bowlingScorecardFetch = dbConnection.createStatement();
		rs = null;
		rs = bowlingScorecardFetch.executeQuery(fetchBowlingPreview);
			
		/* ---------------------------------------------------- Fetch Bowler Scorecard --------------------------------------------------------- */
		while(rs.next())
		{
			Bowler bowler = new Bowler();
			bowler.setBowlerName(rs.getString("player_name"));
			int ballsBowled = rs.getInt("player_balls_bowled");
			int oversBowled = ballsBowled/6;
			int ballsRemainder = ballsBowled%6;
			double actualOversBowled = oversBowled + (ballsRemainder*0.1);
			String oB = actualOversBowled+"";
			double ov = Double.parseDouble(oB.substring(0, oB.indexOf(".")+2));
			bowler.setOversBowled(ov);			
			bowler.setRunsConceded(rs.getInt("player_runs_conceded"));
			bowler.setBowlerWickets(rs.getInt("player_wickets"));
			bowlerList.add(bowler);
		}
			
		/* ---------------------------------------------------- Set up the Scorecard --------------------------------------------------------- */
		masterScorecard.setMatchNumber(matchNumber);
		masterScorecard.setBatsman(batsmanList);
		masterScorecard.setBowler(bowlerList);
	
		return masterScorecard;
	}
	
	
	
	/* ****************************************************** Fetch Score Preview for Current Match ********************************************************* */
	public List<MatchInning> FetchScorePreview(Connection dbConnection, int matchNumber) throws SQLException, IOException
	{
		String fetchScorePreview = "SELECT m.inning_number,m.batting_team,m.fielding_team,m.score,m.wickets,m.overs " +
				"from match_inning m where m.match_number="+matchNumber+" order by inning_number";

		ResultSet rs;
		List<MatchInning> currentMatchPreview = new ArrayList<MatchInning> ();
		
		Statement scorePreviewStatement = dbConnection.createStatement();
		rs = scorePreviewStatement.executeQuery(fetchScorePreview);

		while (rs.next()) 
		{
			MatchInning inning = new MatchInning();
			inning.setInningNumber(rs.getInt("inning_number"));
			inning.setMatchNumber(matchNumber);
			inning.setBattingTeam(rs.getString("batting_team"));
			inning.setFieldingTeam(rs.getString("fielding_team"));
			inning.setScore(rs.getInt("score"));
			inning.setWickets(rs.getInt("wickets"));
			inning.setOvers(rs.getDouble("overs"));
			currentMatchPreview.add(inning);
		}
	
		return currentMatchPreview;
	}

	
	/* ****************************************************** Fetch Stadium Details for Given Stadium ********************************************************* */
	public List<Stadium> FetchStadiumDetails(Connection dbConnection, String stadiumName) throws SQLException, IOException
	{
		Stadium stadium = new Stadium();
		
		String fetchStatement = "select stadium_name,stadium_city,stadium_country,stadium_latitude,stadium_longitude from stadium "+
				"where stadium_name = '"+stadiumName+"'";
		
		ResultSet rs;

		Statement stadiumFetch = dbConnection.createStatement();
		rs = stadiumFetch.executeQuery(fetchStatement);
		
		List<Stadium> stadiumList = new ArrayList<Stadium>();
		
		while(rs.next())
		{
			stadium.setStadiumName(rs.getString("stadium_name"));
			stadium.setStadiumCity(rs.getString("stadium_city"));
			stadium.setStadiumCountry(rs.getString("stadium_country"));
			stadium.setLatitude(rs.getDouble("stadium_latitude"));
			stadium.setLongitude(rs.getDouble("stadium_longitude"));
			stadiumList.add(stadium);
		}
			
		return stadiumList;
	}
	
	
	/* ****************************************************** Fetch Current Day's Match Schedule ********************************************************* */
	public List<MatchSchedule> FetchCurrentDaySchedule(Connection dbConnection) throws SQLException, IOException
	{
		String fetchStatement = "select m.match_number,m.team_one,m.team_two,m.stadium_name,"+ "s.stadium_city,date_format(m.match_timing,'%D %M %Y') as match_date" +
				",match_result from "+ MATCH_SCHEDULE_TABLE+ " m,"+ STADIUM_TABLE+ " s where date(m.match_timing) = date(current_timestamp()) "
				+ "and m.stadium_name = s.stadium_name";
		ResultSet rs;
		
		List<MatchSchedule> currentDayScheduleAL = new ArrayList<MatchSchedule> ();
	
		Statement currentScheduleFetch = dbConnection.createStatement();
		rs = currentScheduleFetch.executeQuery(fetchStatement);

		while (rs.next()) 
		{
			MatchSchedule mSch = new MatchSchedule();
			mSch.setMatchNumber(rs.getInt("match_number"));
			mSch.setTeamOne(rs.getString("team_one"));
			mSch.setTeamTwo(rs.getString("team_two"));
			mSch.setStadiumName(rs.getString("stadium_name") + " stadium");
			mSch.setStadiumCity(rs.getString("stadium_city"));
			mSch.setMatchDate(rs.getString("match_date"));
			mSch.setMatchResult(rs.getString("match_result"));
			currentDayScheduleAL.add(mSch);
		}
	
		return currentDayScheduleAL;
	}

	
	/* ****************************************************** Fetch Future Match Schedules ********************************************************* */
	public List<MatchSchedule> FetchFutureMatchSchedule(Connection dbConnection) throws SQLException, IOException
	{
		String fetchStatement = "select m.match_number,m.team_one,m.team_two,m.stadium_name,"+ "s.stadium_city,date_format(m.match_timing,'%D %M %Y') as match_date" +
				",match_result from "+ MATCH_SCHEDULE_TABLE+ " m,"+ STADIUM_TABLE+ " s where date(m.match_timing) > date(current_timestamp()) "+
				"and m.stadium_name = s.stadium_name";
		
		ResultSet rs;
		List<MatchSchedule> futureSchedule = new ArrayList<MatchSchedule> ();
		
		Statement futureScheduleFetch = dbConnection.createStatement();
		rs = futureScheduleFetch.executeQuery(fetchStatement);

		while (rs.next()) 
		{
			MatchSchedule mSch = new MatchSchedule();
			mSch.setMatchNumber(rs.getInt("match_number"));
			mSch.setTeamOne(rs.getString("team_one"));
			mSch.setTeamTwo(rs.getString("team_two"));
			mSch.setStadiumName(rs.getString("stadium_name"));
			mSch.setStadiumCity(rs.getString("stadium_city"));
			mSch.setMatchDate(rs.getString("match_date"));
			mSch.setMatchResult(rs.getString("match_result"));
			futureSchedule.add(mSch);
		}
		
		return futureSchedule;
	}
	
	
	/* ****************************************************** Update Stadium Details ********************************************************* */
	public int UpdateStadium(Connection dbConnection, Stadium stadium) throws SQLException, IOException
	{
		String updateStatement = "insert into " + STADIUM_TABLE+ " values(?,?,?,?)";
		int success = -1;

		PreparedStatement preparedStatement = dbConnection.prepareStatement(updateStatement);

		preparedStatement.setString(1, "'"+stadium.getStadiumName()+"'");
		preparedStatement.setString(2, "'"+stadium.getStadiumCity()+"'");
		preparedStatement.setString(3, "'"+stadium.getStadiumCountry()+"'");
		preparedStatement.setLong(4, stadium.getStadiumCapacity());

		success = preparedStatement.executeUpdate();

		dbConnection.commit();
		
		return success;
	}

	
	
	/* ****************************************************** Update Player Records ********************************************************* */
	public int UpdatePlayerRecords(Connection dbConnection, MatchOver matchOver) throws SQLException, IOException 
	{
		String updateStatement = "";
		PreparedStatement preparedStatement;

		int success = -1;

		/* ----------------------------------------- Update Player Records for Regular Deliveries --------------------------------------------- */
		if (matchOver.getRunsScored() != 0 && !(matchOver.getAction().equalsIgnoreCase("WD") || matchOver
						.getAction().equalsIgnoreCase("NB"))) 
		{
			// Update Batsman's record
			updateStatement = "UPDATE "+ PLAYER_TABLE+ " SET runs_scored=runs_scored+"+matchOver.getRunsScored()+",balls_played=balls_played+1 " +
					"WHERE player_name = '"+matchOver.getBatsman()+"'";
			
			preparedStatement = dbConnection.prepareStatement(updateStatement);
			success = preparedStatement.executeUpdate();

			// Update Bowler's record
			updateStatement = "UPDATE "+ PLAYER_TABLE+ " SET runs_conceded = runs_conceded+"+matchOver.getRunsScored()+",balls_bowled=balls_bowled+1 " +
					"WHERE player_name='"+matchOver.getBowler()+"'";
				
			preparedStatement = dbConnection.prepareStatement(updateStatement);
			success = preparedStatement.executeUpdate();

			dbConnection.commit();
		} 
		/* ---------------------------------------------- Update Player Records for Wickets -------------------------------------------------- */
		else if (matchOver.getAction().equalsIgnoreCase("W")) 
		{
			//Update Batsman's record
			updateStatement = "UPDATE "+PLAYER_TABLE+" SET runs_scored=runs_scored+"+matchOver.getRunsScored()+",balls_played=balls_played+1 " +
					"where player_name='"+matchOver.getBatsman()+"'";
				
			// Update Bowler's record
			updateStatement = "UPDATE "+ PLAYER_TABLE+ " SET runs_conceded=runs_conceded+"+matchOver.getRunsScored()+",wickets_taken=wickets_taken+1," +
					"balls_bowled=balls_bowled+1 WHERE player_name = '"+matchOver.getBowler()+"'";
				
			preparedStatement = dbConnection.prepareStatement(updateStatement);
			success = preparedStatement.executeUpdate();

			dbConnection.commit();
		} 
		/* ---------------------------------------------- Update Bowler Records for Extras -------------------------------------------------- */
		else
		{
			// Update Bowler's record in case of WD or NB
			updateStatement = "UPDATE "+ PLAYER_TABLE+ " SET runs_conceded=runs_conceded+"+matchOver.getRunsScored()+" WHERE player_name='"+matchOver.getBowler()+"'";
			
			preparedStatement = dbConnection.prepareStatement(updateStatement);
			success = preparedStatement.executeUpdate();

			dbConnection.commit();
		}
		
		return success;
	}
	
	
	/* ***************************************************** Update Match Detailed Scorecard ******************************************************** */
	public int UpdateDetailedScorecard(Connection dbConnection, MatchOver matchover) throws SQLException, IOException 
	{
		String updateStatement = "";

		PreparedStatement preparedStatement = null;

		int success = -1;

		/* ------------------------------------------- Update Player Scorecard for Regular Deliveries --------------------------------------------- */
		if (matchover.getAction().equalsIgnoreCase("R")) 
		{
			if (matchover.getRunsScored() == 6)
			{
				// Update Batsman's scorecard
				updateStatement = "UPDATE "+ SCORECARD_TABLE + " SET player_score=player_score+6, player_balls=player_balls+1, player_sixes=player_sixes+1 "
						+ "WHERE player_name='" + matchover.getBatsman()+"'"
						+ " AND match_number="+ matchover.getMatchNumber();
			} 
			else if (matchover.getRunsScored() == 4)
			{
				// Update Batsman's scorecard
				updateStatement = "UPDATE "+ SCORECARD_TABLE+ " SET player_score=player_score+4, player_balls=player_balls+1, player_fours=player_fours+1 "
						+ "WHERE player_name='"+matchover.getBatsman()+"' AND match_number="+matchover.getMatchNumber();
			}
			else
			{
				// Update Batsman's scorecard
				updateStatement = "UPDATE "+ SCORECARD_TABLE+ " SET player_score=player_score+"+matchover.getRunsScored()+", player_balls=player_balls+1 "
						+ "WHERE player_name='"+matchover.getBatsman()+"' AND match_number="+matchover.getMatchNumber();
			}

			preparedStatement = dbConnection.prepareStatement(updateStatement);
			success = preparedStatement.executeUpdate();

			System.out.println(preparedStatement.toString());
	
			// Update Bowler's scorecard
			updateStatement = "UPDATE "+ SCORECARD_TABLE+ " SET player_balls_bowled=player_balls_bowled+1, player_runs_conceded=player_runs_conceded+"+
			matchover.getRunsScored()+ " where player_name='"+matchover.getBowler()+"' AND match_number="+matchover.getMatchNumber();

			preparedStatement = dbConnection.prepareStatement(updateStatement);
			success = preparedStatement.executeUpdate();

			System.out.println(preparedStatement.toString());

			dbConnection.commit();
		} 
		/* ------------------------------------------- Update Player Scorecard for Wicket Taking Deliveries --------------------------------------------- */
		else if (matchover.getAction().equalsIgnoreCase("W"))
		{
			// Update Batsman's scorecard
			updateStatement = "UPDATE "+ SCORECARD_TABLE+ " SET player_score=player_score+"+matchover.getRunsScored()+", player_balls=player_balls+1," +
					"player_status='O' WHERE player_name='"+matchover.getBatsman()+"' AND match_number="+matchover.getMatchNumber();

			preparedStatement = dbConnection.prepareStatement(updateStatement);
			success = preparedStatement.executeUpdate();

			// Update Bowler's scorecard
			updateStatement = "UPDATE "+ SCORECARD_TABLE+ " SET player_balls_bowled=player_balls_bowled+1, player_runs_conceded=player_runs_conceded+"+
			matchover.getRunsScored()+ ", player_wickets=player_wickets+1 where player_name='"+matchover.getBowler()+"' AND match_number="+
					matchover.getMatchNumber();

			preparedStatement = dbConnection.prepareStatement(updateStatement);
			success = preparedStatement.executeUpdate();

			dbConnection.commit();
		}
		/* ------------------------------------------- Update Player Scorecard for Extras --------------------------------------------- */
		else 
		{
			// Update Bowler's scorecard for Wide (WD), No ball (NB)
			updateStatement = "UPDATE "+ SCORECARD_TABLE+ " SET player_runs_conceded=player_runs_conceded+"+matchover.getRunsScored()
					+ " where player_name='"+matchover.getBowler()+"' AND match_number="+matchover.getMatchNumber();

			preparedStatement = dbConnection.prepareStatement(updateStatement);
			success = preparedStatement.executeUpdate();

			dbConnection.commit();
		}
	
		return success;
	}
	
	
/* ****************************************************** Fetch News Feeds ********************************************************* */
	
	public List<News> FetchNews(Connection dbConnection) throws SQLException, IOException, SQLRecoverableException, SQLNonTransientException
	{
		String fetchStatement = "select news_id,news_headline,news_content,date(news_time) as news_date from news order by news_id desc";
		ResultSet rs;
		
		List<News> newsFeed = new ArrayList<News> ();
	
		Statement currentScheduleFetch = dbConnection.createStatement();
		rs = currentScheduleFetch.executeQuery(fetchStatement);

		while (rs.next()) 
		{
			News story = new News();
			story.setNewsId(rs.getInt("news_id"));
			story.setNewsHeadline(rs.getString("news_headline"));
			story.setNewsContent(rs.getString("news_content"));
			story.setNewsTime(rs.getString("news_date"));
			newsFeed.add(story);
		}
	
		return newsFeed;
	}
	
	
/* ****************************************************** Fetch Complete Story ********************************************************* */
	
	public List<News> FetchDetailedStory(Connection dbConnection, String headline) throws SQLException, IOException, SQLRecoverableException,
			SQLNonTransientException
	{
		String fetchStatement = "select news_id,news_headline,news_content,date(news_time) as news_date from news " +
				"where news_headline = '"+headline+"'";
		ResultSet rs;
		
		List<News> newsFeed = new ArrayList<News> ();
	
		Statement currentScheduleFetch = dbConnection.createStatement();
		rs = currentScheduleFetch.executeQuery(fetchStatement);

		while (rs.next()) 
		{
			News story = new News();
			story.setNewsId(rs.getInt("news_id"));
			story.setNewsHeadline(rs.getString("news_headline"));
			story.setNewsContent(rs.getString("news_content"));
			story.setNewsTime(rs.getString("news_date"));
			newsFeed.add(story);
		}
	
		return newsFeed;
	}
	
	
	/* ****************************************************** Fetch Match Result ********************************************************* */
	
	public List<MatchSchedule> FetchResults(Connection dbConnection) throws SQLException, IOException
	{
		String fetchStatement = "select m.match_number,m.team_one,m.team_two,m.stadium_name,"+ "s.stadium_city,date_format(m.match_timing,'%D %M %Y') as match_date" +
				",match_result from "+ MATCH_SCHEDULE_TABLE+ " m,"+ STADIUM_TABLE+ " s where date(m.match_timing) <= date(current_timestamp()) "
				+ "and m.stadium_name = s.stadium_name and match_result not in('TBA')";
		ResultSet rs;
		
		List<MatchSchedule> currentDayScheduleAL = new ArrayList<MatchSchedule> ();
	
		Statement currentScheduleFetch = dbConnection.createStatement();
		rs = currentScheduleFetch.executeQuery(fetchStatement);

		while (rs.next()) 
		{
			MatchSchedule mSch = new MatchSchedule();
			mSch.setMatchNumber(rs.getInt("match_number"));
			mSch.setTeamOne(rs.getString("team_one"));
			mSch.setTeamTwo(rs.getString("team_two"));
			mSch.setStadiumName(rs.getString("stadium_name") + " stadium");
			mSch.setStadiumCity(rs.getString("stadium_city"));
			mSch.setMatchDate(rs.getString("match_date"));
			mSch.setMatchResult(rs.getString("match_result"));
			currentDayScheduleAL.add(mSch);
		}
		
		return currentDayScheduleAL;
	}
	
	
	/* ****************************************************** Update Match Result ********************************************************* */
	public int UpdateMatchResult(Connection dbConnection, int matchNumber) throws SQLException, IOException
	{
		int success = -1;
		
		String fetchScoreStatement = "SELECT * from "+MATCH_INNINGS_TABLE+" where match_number="+matchNumber+" order by inning_number";
		
		ResultSet rs;
		MatchInning[] inning = new MatchInning[2];
		int inningCount = 0;
		String resultString = "";
		
		Statement fetchFinalScores = dbConnection.createStatement();
		rs = fetchFinalScores.executeQuery(fetchScoreStatement);
			
		while(rs.next())
		{
			inning[inningCount] = new MatchInning();
			inning[inningCount].setMatchNumber(matchNumber);
			inning[inningCount].setInningNumber(rs.getInt("inning_number"));
			inning[inningCount].setBattingTeam(rs.getString("batting_team"));
			inning[inningCount].setFieldingTeam(rs.getString("fielding_team"));
			inning[inningCount].setScore(rs.getInt("score"));
			inning[inningCount].setOvers(rs.getDouble("overs"));
			inning[inningCount].setWickets(rs.getInt("wickets"));
			inningCount++;
		}
			
		if(((inning[0].getScore() > inning[1].getScore()) && (inning[1].getOvers() == 50.0)) ||
				((inning[0].getScore() > inning[1].getScore()) && (inning[1].getWickets() >= 10)))
		{
			resultString = inning[0].getBattingTeam()+" won by "+(inning[0].getScore() - inning[1].getScore())+" runs";
		}
		else if((inning[0].getScore() < inning[1].getScore()) && inning[1].getWickets() < 10)
		{
			resultString = inning[1].getBattingTeam()+" won by "+(10 - inning[1].getWickets())+" wickets";
		}
		else
		{
			resultString = "Match still in progress";
		}
			
		String updateStatement = "UPDATE "+MATCH_SCHEDULE_TABLE+" SET match_result='"+resultString+"' where match_number="+matchNumber;
		
		PreparedStatement resultUpdate = dbConnection.prepareStatement(updateStatement);
		success = resultUpdate.executeUpdate();
		
		dbConnection.commit();
			
		return success;
	}
	
	
	/* ****************************************************** Update Match Over ********************************************************* */
	public MatchInning UpdateOver(Connection dbConnection, MatchOver matchover) throws SQLException, IOException
	{
		String updateStatement = "INSERT INTO " + MATCH_OVER_TABLE+ " VALUES(?,?,0,sysdate(),?,?,?,?,?)";
		int success = -1;

		PreparedStatement preparedStatement = dbConnection.prepareStatement(updateStatement);

		preparedStatement.setInt(1, matchover.getMatchNumber());
		preparedStatement.setInt(2, matchover.getInningNumber());
		//preparedStatement.setInt(3, matchover.getBallNumber());
		preparedStatement.setString(3, "'"+matchover.getAction()+"'");
		preparedStatement.setString(4, "'"+matchover.getBatsman()+"'");
		preparedStatement.setString(5, "'"+matchover.getBowler()+"'");
		preparedStatement.setString(6, "'"+matchover.getComment()+"'");
		preparedStatement.setInt(7, matchover.getRunsScored());

		success = preparedStatement.executeUpdate();

		dbConnection.commit();
		
		// Update Current Match's Scorecard
		success = UpdateDetailedScorecard(dbConnection, matchover);

		// Update Current Inning Summary
		MatchInning matchInning = UpdateInningDuringMatch(dbConnection, matchover);

		// Update Player International Records
		success = UpdatePlayerRecords(dbConnection, matchover);
			
		return matchInning;
	}
}
