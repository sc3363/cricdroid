package edu.columbia.sportsleagueandroid.appengine.beans;

public class Team 
{
	private String teamName;
	private int teamRanking;
	private int matchCount;
	private int wins;
	private int losses;
	private int noResult;
	
	public String getTeamName()
	{
		return teamName;
	}
	
	public void setTeamName(String teamName) 
	{
		this.teamName = teamName;
	}
	
	public int getTeamRanking() 
	{
		return teamRanking;
	}
	
	public void setTeamRanking(int teamRanking) 
	{
		this.teamRanking = teamRanking;
	}
	
	public int getMatchCount() 
	{
		return matchCount;
	}
	
	public void setMatchCount(int matchCount) 
	{
		this.matchCount = matchCount;
	}
	
	public int getWins() 
	{
		return wins;
	}
	
	public void setWins(int wins) 
	{
		this.wins = wins;
	}
	
	public int getLosses() 
	{
		return losses;
	}
	
	public void setLosses(int losses) 
	{
		this.losses = losses;
	}
	
	public int getNoResult() 
	{
		return noResult;
	}
	
	public void setNoResult(int noResult)
	{
		this.noResult = noResult;
	}
	
	
}
