package edu.columbia.sportsleagueandroid.shared;

import com.google.web.bindery.requestfactory.shared.ProxyForName;
import com.google.web.bindery.requestfactory.shared.ValueProxy;

@ProxyForName(value = "edu.columbia.sportsleagueandroid.server.MatchInning", locator = "edu.columbia.sportsleagueandroid.server.MatchInningLocator")
public interface MatchInningProxy extends ValueProxy {

	int getMatchNumber();

	void setMatchNumber(int matchNumber);

	int getInningNumber();

	void setInningNumber(int inningNumber);

	String getBattingTeam();

	void setBattingTeam(String battingTeam);

	String getFieldingTeam();

	void setFieldingTeam(String fieldingTeam);

	int getScore();

	void setScore(int score);

	int getWickets();

	void setWickets(int wickets);

	double getOvers();

	void setOvers(double overs);

}
