package edu.columbia.sportsleagueandroid.appengine.beans;

public class Stadium 
{
	private String stadiumName;
	private String stadiumCity;
	private String stadiumCountry;
	private long stadiumCapacity;
	
	public String getStadiumName() 
	{
		return stadiumName;
	}
	
	public void setStadiumName(String stadiumName)
	{
		this.stadiumName = stadiumName;
	}
	
	public String getStadiumCity()
	{
		return stadiumCity;
	}
	
	public void setStadiumCity(String stadiumCity)
	{
		this.stadiumCity = stadiumCity;
	}
	
	public String getStadiumCountry() 
	{
		return stadiumCountry;
	}
	
	public void setStadiumCountry(String stadiumCountry) 
	{
		this.stadiumCountry = stadiumCountry;
	}
	
	public long getStadiumCapacity() 
	{
		return stadiumCapacity;
	}
	
	public void setStadiumCapacity(long stadiumCapacity)
	{
		this.stadiumCapacity = stadiumCapacity;
	}
	
}
