package edu.columbia.sportsleagueandroid.server;

import com.google.web.bindery.requestfactory.shared.Locator;


public class BatsmanLocator extends Locator<Batsman, Void> {

	@Override
	public Batsman create(Class<? extends Batsman> clazz) {
		return new Batsman();
	}

	@Override
	public Batsman find(Class<? extends Batsman> clazz, Void id) {
		return create(clazz);
	}

	@Override
	public Class<Batsman> getDomainType() {
		return Batsman.class;
	}

	@Override
	public Void getId(Batsman domainObject) {
		return null;
	}

	@Override
	public Class<Void> getIdType() {
		return Void.class;
	}

	@Override
	public Object getVersion(Batsman domainObject) {
		return null;
	}

}
