package edu.columbia.sportsleagueandroid.shared;

import java.sql.Connection;
import java.util.List;

import com.google.web.bindery.requestfactory.shared.Request;
import com.google.web.bindery.requestfactory.shared.RequestContext;
import com.google.web.bindery.requestfactory.shared.ServiceName;

@ServiceName(value = "edu.columbia.sportsleagueandroid.server.SportsLeagueAndroidService", locator = "edu.columbia.sportsleagueandroid.server.SportsLeagueAndroidServiceLocator")
public interface SportsLeagueAndroidRequest extends RequestContext {

	Request<BatsmanProxy> createBatsman();

	Request<BatsmanProxy> readBatsman(Long id);

	Request<BatsmanProxy> updateBatsman(String email_id, int match_no, String pref);

	Request<Void> deleteBatsman(BatsmanProxy batsman);

	Request<List<String>> queryBatsmans(int matchno, String email_id);

	Request<MatchOverProxy> createMatchOver();

	Request<MatchOverProxy> readMatchOver();

	Request<MatchOverProxy> updateMatchOver(MatchOverProxy matchover);

	Request<Void> deleteMatchOver(MatchOverProxy matchover);

	Request<List<MatchOverProxy>> queryMatchOvers();

	Request<BowlerProxy> createBowler();

	Request<BowlerProxy> readBowler(Long id);

	Request<BowlerProxy> updateBowler(BowlerProxy bowler);

	Request<Void> deleteBowler(BowlerProxy bowler);

	Request<List<BowlerProxy>> queryBowlers();

	Request<MatchInningProxy> createMatchInning();

	Request<MatchInningProxy> readMatchInning(Long id);

	Request<MatchInningProxy> updateMatchInning(MatchInningProxy matchinning);

	Request<Void> deleteMatchInning(MatchInningProxy matchinning);

	Request<List<MatchInningProxy>> queryMatchInnings(int matchNumber);

	Request<TeamProxy> createTeam();

	Request<TeamProxy> readTeam(Long id);

	Request<TeamProxy> updateTeam(TeamProxy team);

	Request<Void> deleteTeam(TeamProxy team);

	Request<List<TeamProxy>> queryTeams();

	Request<MasterScorecardProxy> createMasterScorecard();

	Request<MasterScorecardProxy> readMasterScorecard(Long id);

	Request<MasterScorecardProxy> updateMasterScorecard(
			MasterScorecardProxy masterscorecard);

	Request<Void> deleteMasterScorecard(MasterScorecardProxy masterscorecard);

	Request<List<MasterScorecardProxy>> queryMasterScorecards(final int matchNumber, final int inningNumber, final String battingTeam, final String fieldingTeam, 
			final int teamScore, final int teamWickets, final double teamOvers, final String stadiumName,final String stadiumCity);

	Request<StadiumProxy> createStadium();

	Request<StadiumProxy> readStadium(Long id);

	Request<StadiumProxy> updateStadium(StadiumProxy stadium);

	Request<Void> deleteStadium(StadiumProxy stadium);

	Request<List<StadiumProxy>> queryStadiums(String stadiumName);

	Request<PlayerProxy> createPlayer();

	Request<PlayerProxy> readPlayer(Long id);

	Request<PlayerProxy> updatePlayer(PlayerProxy player);

	Request<Void> deletePlayer(PlayerProxy player);

	Request<List<PlayerProxy>> queryPlayers();

	Request<NewsProxy> createNews();

	Request<NewsProxy> readNews(Long id);

	Request<NewsProxy> updateNews(NewsProxy news);

	Request<Void> deleteNews(NewsProxy news);

	Request<List<NewsProxy>> queryNewss(String headline);

	Request<MatchScheduleProxy> createMatchSchedule();

	Request<MatchScheduleProxy> readMatchSchedule(Long id);

	Request<MatchScheduleProxy> updateMatchSchedule(
			MatchScheduleProxy matchschedule);

	Request<Void> deleteMatchSchedule(MatchScheduleProxy matchschedule);

	Request<List<MatchScheduleProxy>> queryMatchSchedules(String type);

}
