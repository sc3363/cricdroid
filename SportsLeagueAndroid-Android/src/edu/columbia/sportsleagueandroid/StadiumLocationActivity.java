package edu.columbia.sportsleagueandroid;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;

import com.google.web.bindery.requestfactory.shared.Receiver;
import com.google.web.bindery.requestfactory.shared.ServerFailure;

import edu.columbia.sportsleagueandroid.client.MyRequestFactory;
import edu.columbia.sportsleagueandroid.shared.StadiumProxy;

public class StadiumLocationActivity extends Activity 
{	
	private static boolean BACK_BUTTON_PRESSED_FROM_CHILD = false;
	
	@Override
	public void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.fixtures_layout);
	}
	
	public void onResume()
	{
		super.onResume();
	
		if(!BACK_BUTTON_PRESSED_FROM_CHILD)
		{	
			setScreenContent();
		}
		else
		{
			finish();
		}
	}

//	public void finishFromChild(LocateStadiumOnMapActivity locateStadiumOnMapActivity) 
//	{
//		// TODO Auto-generated method stub
//		super.finishFromChild(locateStadiumOnMapActivity);
//		BACK_BUTTON_PRESSED_FROM_CHILD = true;
//		finish();
//	}
	
	@Override
	public void onDestroy()
	{
		super.onDestroy();
	}
	
//	@Override
//	protected void onActivityResult(int requestCode, int resultCode, Intent data) 
//	{
//		// TODO Auto-generated method stub
//		super.onActivityResult(requestCode, resultCode, data);
//		if(resultCode == RESULT_CANCELED || resultCode == RESULT_OK)
//		{
//			BACK_BUTTON_PRESSED_FROM_CHILD = true;
//		}
//	}
	
	public void setScreenContent()
	{
		String extraString = getIntent().getExtras().getString("extra");
		String[] matchStarterData = extraString.split("[\\|]");
		
		int matchNumber = Integer.parseInt(matchStarterData[0].substring(matchStarterData[0].indexOf("#")+2, matchStarterData[0].length()-1));
		String[] residualData = matchStarterData[1].split("' \n");
		String[] residualMatchData = residualData[0].split("\n");
		final String matchDate = residualMatchData[0].trim();
		String teams[] = residualMatchData[1].split(" vs ");
		final String teamOne = teams[0].replace("'", "").replace(" ", "");
		final String teamTwo = teams[1];
		String stadiumResidual[] = residualMatchData[2].split(", ");
		final String stadiumName = stadiumResidual[0];
		String stadiumCity = stadiumResidual[1];
		
		final List<StadiumProxy> stadiumList = new ArrayList<StadiumProxy>();
		// Use an AsyncTask to avoid blocking the UI thread
		new AsyncTask<Void, Void, List<StadiumProxy>>() 
		{
			@Override
			protected List<StadiumProxy> doInBackground(Void... arg0) 
			{
				MyRequestFactory requestFactory = Util.getRequestFactory(getBaseContext(), MyRequestFactory.class);
						
				requestFactory.sportsLeagueAndroidRequest().queryStadiums(stadiumName).fire(new Receiver<List<StadiumProxy>>() 
				{
					@Override
					public void onFailure(ServerFailure error)
					{
						String message = "Failure: "+ error.getMessage();
					}

					@Override
					public void onSuccess(List<StadiumProxy> stadiumData) 
					{
						stadiumList.addAll(stadiumData);
					}
				});
				
				return stadiumList;
			}
			
			@Override
			protected void onPostExecute(final List<StadiumProxy> stadiumProxy)
			{
				String[] stadiumName = {stadiumProxy.get(0).getStadiumName()};
				String[] stadiumCity = {stadiumProxy.get(0).getStadiumCity()};
				double[] latitude = {stadiumProxy.get(0).getLatitude()* Math.pow(10,6)};
				double[] longitude = {stadiumProxy.get(0).getLongitude()* Math.pow(10,6)};
				String[] team1 = {teamOne};
				String[] team2 = {teamTwo};
				String[] date = {matchDate};
				
				Intent locateStadiumOnMapIntent = new Intent(StadiumLocationActivity.this,LocateStadiumOnMapActivity.class);
				LocateStadiumOnMapActivity.feedLocation(stadiumName, stadiumCity, team1, team2, date, latitude, longitude);
				startActivityForResult(locateStadiumOnMapIntent, 0);
				finish();
			}
		}.execute();
	}
}
