// Automatically Generated -- DO NOT EDIT
// edu.columbia.sportsleagueandroid.client.MyRequestFactory
package edu.columbia.sportsleagueandroid.client;
import java.util.Arrays;
import com.google.web.bindery.requestfactory.vm.impl.OperationData;
import com.google.web.bindery.requestfactory.vm.impl.OperationKey;
public final class MyRequestFactoryDeobfuscatorBuilder extends com.google.web.bindery.requestfactory.vm.impl.Deobfuscator.Builder {
{
withOperation(new OperationKey("AvemNfrSPgIlkSUiW2iS1__MDPw="),
  new OperationData.Builder()
  .withClientMethodDescriptor("()Lcom/google/web/bindery/requestfactory/shared/Request;")
  .withDomainMethodDescriptor("()Ledu/columbia/sportsleagueandroid/server/MatchSchedule;")
  .withMethodName("createMatchSchedule")
  .withRequestContext("edu.columbia.sportsleagueandroid.shared.SportsLeagueAndroidRequest")
  .build());
withOperation(new OperationKey("TzNGKaKTN2rrcEUBuXmbvaueNpM="),
  new OperationData.Builder()
  .withClientMethodDescriptor("()Lcom/google/web/bindery/requestfactory/shared/Request;")
  .withDomainMethodDescriptor("()Ledu/columbia/sportsleagueandroid/server/MasterScorecard;")
  .withMethodName("createMasterScorecard")
  .withRequestContext("edu.columbia.sportsleagueandroid.shared.SportsLeagueAndroidRequest")
  .build());
withOperation(new OperationKey("f5toKxyapI3heI9uMNd3kPoCuBQ="),
  new OperationData.Builder()
  .withClientMethodDescriptor("()Lcom/google/web/bindery/requestfactory/shared/Request;")
  .withDomainMethodDescriptor("()Ledu/columbia/sportsleagueandroid/server/MatchOver;")
  .withMethodName("createMatchOver")
  .withRequestContext("edu.columbia.sportsleagueandroid.shared.SportsLeagueAndroidRequest")
  .build());
withOperation(new OperationKey("o19dj0WsQ6Df_sVmTpWb_1rHJME="),
  new OperationData.Builder()
  .withClientMethodDescriptor("(Ledu/columbia/sportsleagueandroid/shared/MasterScorecardProxy;)Lcom/google/web/bindery/requestfactory/shared/Request;")
  .withDomainMethodDescriptor("(Ledu/columbia/sportsleagueandroid/server/MasterScorecard;)Ledu/columbia/sportsleagueandroid/server/MasterScorecard;")
  .withMethodName("updateMasterScorecard")
  .withRequestContext("edu.columbia.sportsleagueandroid.shared.SportsLeagueAndroidRequest")
  .build());
withOperation(new OperationKey("QQQWh4XGkeUP2KPiUjAbkbcF8WQ="),
  new OperationData.Builder()
  .withClientMethodDescriptor("()Lcom/google/web/bindery/requestfactory/shared/Request;")
  .withDomainMethodDescriptor("()Ledu/columbia/sportsleagueandroid/server/News;")
  .withMethodName("createNews")
  .withRequestContext("edu.columbia.sportsleagueandroid.shared.SportsLeagueAndroidRequest")
  .build());
withOperation(new OperationKey("CqojPH8DUMTp9hgHNnRs7ot_KEc="),
  new OperationData.Builder()
  .withClientMethodDescriptor("(I)Lcom/google/web/bindery/requestfactory/shared/Request;")
  .withDomainMethodDescriptor("(I)Ljava/util/List;")
  .withMethodName("queryMatchInnings")
  .withRequestContext("edu.columbia.sportsleagueandroid.shared.SportsLeagueAndroidRequest")
  .build());
withOperation(new OperationKey("JyWCeooMUUx3NwrEkhqbb5Q2WGc="),
  new OperationData.Builder()
  .withClientMethodDescriptor("(Ledu/columbia/sportsleagueandroid/shared/MatchInningProxy;)Lcom/google/web/bindery/requestfactory/shared/Request;")
  .withDomainMethodDescriptor("(Ledu/columbia/sportsleagueandroid/server/MatchInning;)Ledu/columbia/sportsleagueandroid/server/MatchInning;")
  .withMethodName("updateMatchInning")
  .withRequestContext("edu.columbia.sportsleagueandroid.shared.SportsLeagueAndroidRequest")
  .build());
withOperation(new OperationKey("AMVPK4p018PTY9exInjcb2HePXU="),
  new OperationData.Builder()
  .withClientMethodDescriptor("(Ledu/columbia/sportsleagueandroid/shared/BowlerProxy;)Lcom/google/web/bindery/requestfactory/shared/Request;")
  .withDomainMethodDescriptor("(Ledu/columbia/sportsleagueandroid/server/Bowler;)V")
  .withMethodName("deleteBowler")
  .withRequestContext("edu.columbia.sportsleagueandroid.shared.SportsLeagueAndroidRequest")
  .build());
withOperation(new OperationKey("XN8VL2bW4SCrpCxJGnPTGYQmQcA="),
  new OperationData.Builder()
  .withClientMethodDescriptor("(Ljava/lang/String;)Lcom/google/web/bindery/requestfactory/shared/Request;")
  .withDomainMethodDescriptor("(Ljava/lang/String;)Ljava/util/List;")
  .withMethodName("queryNewss")
  .withRequestContext("edu.columbia.sportsleagueandroid.shared.SportsLeagueAndroidRequest")
  .build());
withOperation(new OperationKey("xm$m6OTjwJZOra8wgCG5FdT_weY="),
  new OperationData.Builder()
  .withClientMethodDescriptor("(Ledu/columbia/sportsleagueandroid/shared/MatchOverProxy;)Lcom/google/web/bindery/requestfactory/shared/Request;")
  .withDomainMethodDescriptor("(Ledu/columbia/sportsleagueandroid/server/MatchOver;)Ledu/columbia/sportsleagueandroid/server/MatchOver;")
  .withMethodName("updateMatchOver")
  .withRequestContext("edu.columbia.sportsleagueandroid.shared.SportsLeagueAndroidRequest")
  .build());
withOperation(new OperationKey("mdakLn0udztOra4RCh3rWNVCBGI="),
  new OperationData.Builder()
  .withClientMethodDescriptor("()Lcom/google/web/bindery/requestfactory/shared/Request;")
  .withDomainMethodDescriptor("()Ljava/util/List;")
  .withMethodName("queryBowlers")
  .withRequestContext("edu.columbia.sportsleagueandroid.shared.SportsLeagueAndroidRequest")
  .build());
withOperation(new OperationKey("oiAgn9HOjtQ2TO_LENLAKzTl78M="),
  new OperationData.Builder()
  .withClientMethodDescriptor("(Ledu/columbia/sportsleagueandroid/shared/BowlerProxy;)Lcom/google/web/bindery/requestfactory/shared/Request;")
  .withDomainMethodDescriptor("(Ledu/columbia/sportsleagueandroid/server/Bowler;)Ledu/columbia/sportsleagueandroid/server/Bowler;")
  .withMethodName("updateBowler")
  .withRequestContext("edu.columbia.sportsleagueandroid.shared.SportsLeagueAndroidRequest")
  .build());
withOperation(new OperationKey("I_NzcYxdid7wUk9erwVHbNjAcbI="),
  new OperationData.Builder()
  .withClientMethodDescriptor("()Lcom/google/web/bindery/requestfactory/shared/Request;")
  .withDomainMethodDescriptor("()Ledu/columbia/sportsleagueandroid/server/Stadium;")
  .withMethodName("createStadium")
  .withRequestContext("edu.columbia.sportsleagueandroid.shared.SportsLeagueAndroidRequest")
  .build());
withOperation(new OperationKey("ZPbxDaBcVZ1pBPCK_UMLCBG1EFU="),
  new OperationData.Builder()
  .withClientMethodDescriptor("(Ljava/lang/Long;)Lcom/google/web/bindery/requestfactory/shared/Request;")
  .withDomainMethodDescriptor("(Ljava/lang/Long;)Ledu/columbia/sportsleagueandroid/server/MatchSchedule;")
  .withMethodName("readMatchSchedule")
  .withRequestContext("edu.columbia.sportsleagueandroid.shared.SportsLeagueAndroidRequest")
  .build());
withOperation(new OperationKey("XP_MgXpdJp$MHxiuVcErjrHbKGA="),
  new OperationData.Builder()
  .withClientMethodDescriptor("(Ljava/lang/Long;)Lcom/google/web/bindery/requestfactory/shared/Request;")
  .withDomainMethodDescriptor("(Ljava/lang/Long;)Ledu/columbia/sportsleagueandroid/server/Team;")
  .withMethodName("readTeam")
  .withRequestContext("edu.columbia.sportsleagueandroid.shared.SportsLeagueAndroidRequest")
  .build());
withOperation(new OperationKey("_pBTi5htzeK4fH1RVQGsqqVxZT8="),
  new OperationData.Builder()
  .withClientMethodDescriptor("()Lcom/google/web/bindery/requestfactory/shared/Request;")
  .withDomainMethodDescriptor("()Ledu/columbia/sportsleagueandroid/server/MatchInning;")
  .withMethodName("createMatchInning")
  .withRequestContext("edu.columbia.sportsleagueandroid.shared.SportsLeagueAndroidRequest")
  .build());
withOperation(new OperationKey("tiLfvJAGLWfkWAWp3wrDFmLm9H0="),
  new OperationData.Builder()
  .withClientMethodDescriptor("()Lcom/google/web/bindery/requestfactory/shared/Request;")
  .withDomainMethodDescriptor("()Ljava/util/List;")
  .withMethodName("queryTeams")
  .withRequestContext("edu.columbia.sportsleagueandroid.shared.SportsLeagueAndroidRequest")
  .build());
withOperation(new OperationKey("Q6ptCnozVuDinwDP4LQKMCkVB60="),
  new OperationData.Builder()
  .withClientMethodDescriptor("(Ledu/columbia/sportsleagueandroid/shared/PlayerProxy;)Lcom/google/web/bindery/requestfactory/shared/Request;")
  .withDomainMethodDescriptor("(Ledu/columbia/sportsleagueandroid/server/Player;)V")
  .withMethodName("deletePlayer")
  .withRequestContext("edu.columbia.sportsleagueandroid.shared.SportsLeagueAndroidRequest")
  .build());
withOperation(new OperationKey("p_V7Ac4pg80qeqYJSMgwN_h5ffA="),
  new OperationData.Builder()
  .withClientMethodDescriptor("()Lcom/google/web/bindery/requestfactory/shared/Request;")
  .withDomainMethodDescriptor("()Ledu/columbia/sportsleagueandroid/server/MatchOver;")
  .withMethodName("readMatchOver")
  .withRequestContext("edu.columbia.sportsleagueandroid.shared.SportsLeagueAndroidRequest")
  .build());
withOperation(new OperationKey("xBvDeM8wW7ZtOv$jHw5eqOQdCig="),
  new OperationData.Builder()
  .withClientMethodDescriptor("(Ljava/lang/Long;)Lcom/google/web/bindery/requestfactory/shared/Request;")
  .withDomainMethodDescriptor("(Ljava/lang/Long;)Ledu/columbia/sportsleagueandroid/server/Player;")
  .withMethodName("readPlayer")
  .withRequestContext("edu.columbia.sportsleagueandroid.shared.SportsLeagueAndroidRequest")
  .build());
withOperation(new OperationKey("GHl6RzUc$NDMn1$YSec6dg83Xso="),
  new OperationData.Builder()
  .withClientMethodDescriptor("(Ljava/lang/Long;)Lcom/google/web/bindery/requestfactory/shared/Request;")
  .withDomainMethodDescriptor("(Ljava/lang/Long;)Ledu/columbia/sportsleagueandroid/server/Batsman;")
  .withMethodName("readBatsman")
  .withRequestContext("edu.columbia.sportsleagueandroid.shared.SportsLeagueAndroidRequest")
  .build());
withOperation(new OperationKey("Bps8pcrIvnnU14w6aNgesKa$v8o="),
  new OperationData.Builder()
  .withClientMethodDescriptor("()Lcom/google/web/bindery/requestfactory/shared/Request;")
  .withDomainMethodDescriptor("()Ledu/columbia/sportsleagueandroid/server/Bowler;")
  .withMethodName("createBowler")
  .withRequestContext("edu.columbia.sportsleagueandroid.shared.SportsLeagueAndroidRequest")
  .build());
withOperation(new OperationKey("e8Y$2s5E0AVje2wIPERerV7K8wY="),
  new OperationData.Builder()
  .withClientMethodDescriptor("(ILjava/lang/String;)Lcom/google/web/bindery/requestfactory/shared/Request;")
  .withDomainMethodDescriptor("(ILjava/lang/String;)Ljava/util/List;")
  .withMethodName("queryBatsmans")
  .withRequestContext("edu.columbia.sportsleagueandroid.shared.SportsLeagueAndroidRequest")
  .build());
withOperation(new OperationKey("F9anF9i2N_JjIhubmNoGtlw9jzs="),
  new OperationData.Builder()
  .withClientMethodDescriptor("()Lcom/google/web/bindery/requestfactory/shared/Request;")
  .withDomainMethodDescriptor("()Ledu/columbia/sportsleagueandroid/server/Team;")
  .withMethodName("createTeam")
  .withRequestContext("edu.columbia.sportsleagueandroid.shared.SportsLeagueAndroidRequest")
  .build());
withOperation(new OperationKey("vy$uRC1BdAA4W4Xbv9QeG1xrojo="),
  new OperationData.Builder()
  .withClientMethodDescriptor("(Ledu/columbia/sportsleagueandroid/shared/TeamProxy;)Lcom/google/web/bindery/requestfactory/shared/Request;")
  .withDomainMethodDescriptor("(Ledu/columbia/sportsleagueandroid/server/Team;)V")
  .withMethodName("deleteTeam")
  .withRequestContext("edu.columbia.sportsleagueandroid.shared.SportsLeagueAndroidRequest")
  .build());
withOperation(new OperationKey("uNE48NymyfQ6bZSAO4VfJF6hrFI="),
  new OperationData.Builder()
  .withClientMethodDescriptor("(Ledu/columbia/sportsleagueandroid/shared/MatchScheduleProxy;)Lcom/google/web/bindery/requestfactory/shared/Request;")
  .withDomainMethodDescriptor("(Ledu/columbia/sportsleagueandroid/server/MatchSchedule;)V")
  .withMethodName("deleteMatchSchedule")
  .withRequestContext("edu.columbia.sportsleagueandroid.shared.SportsLeagueAndroidRequest")
  .build());
withOperation(new OperationKey("rpc4uXbwOGTjXjA6JDZ4nFwwPS0="),
  new OperationData.Builder()
  .withClientMethodDescriptor("(Ledu/columbia/sportsleagueandroid/shared/StadiumProxy;)Lcom/google/web/bindery/requestfactory/shared/Request;")
  .withDomainMethodDescriptor("(Ledu/columbia/sportsleagueandroid/server/Stadium;)V")
  .withMethodName("deleteStadium")
  .withRequestContext("edu.columbia.sportsleagueandroid.shared.SportsLeagueAndroidRequest")
  .build());
withOperation(new OperationKey("Fa9JviN1JhZ9cqxxJCTdRykWwlM="),
  new OperationData.Builder()
  .withClientMethodDescriptor("()Lcom/google/web/bindery/requestfactory/shared/Request;")
  .withDomainMethodDescriptor("()Ljava/util/List;")
  .withMethodName("queryMatchOvers")
  .withRequestContext("edu.columbia.sportsleagueandroid.shared.SportsLeagueAndroidRequest")
  .build());
withOperation(new OperationKey("v7_uvCz4ZseR4MZRfTdxuirGV54="),
  new OperationData.Builder()
  .withClientMethodDescriptor("(Ledu/columbia/sportsleagueandroid/shared/MatchScheduleProxy;)Lcom/google/web/bindery/requestfactory/shared/Request;")
  .withDomainMethodDescriptor("(Ledu/columbia/sportsleagueandroid/server/MatchSchedule;)Ledu/columbia/sportsleagueandroid/server/MatchSchedule;")
  .withMethodName("updateMatchSchedule")
  .withRequestContext("edu.columbia.sportsleagueandroid.shared.SportsLeagueAndroidRequest")
  .build());
withOperation(new OperationKey("UklnjpGVXwoVCMC4QqlciCdkzZI="),
  new OperationData.Builder()
  .withClientMethodDescriptor("(Ledu/columbia/sportsleagueandroid/shared/NewsProxy;)Lcom/google/web/bindery/requestfactory/shared/Request;")
  .withDomainMethodDescriptor("(Ledu/columbia/sportsleagueandroid/server/News;)Ledu/columbia/sportsleagueandroid/server/News;")
  .withMethodName("updateNews")
  .withRequestContext("edu.columbia.sportsleagueandroid.shared.SportsLeagueAndroidRequest")
  .build());
withOperation(new OperationKey("FumfcjJhZlS_ctw2TaoSAmcBy7g="),
  new OperationData.Builder()
  .withClientMethodDescriptor("(Ljava/lang/String;)Lcom/google/web/bindery/requestfactory/shared/Request;")
  .withDomainMethodDescriptor("(Ljava/lang/String;)Ljava/util/List;")
  .withMethodName("queryStadiums")
  .withRequestContext("edu.columbia.sportsleagueandroid.shared.SportsLeagueAndroidRequest")
  .build());
withOperation(new OperationKey("bbdWl55CED$V$7HBF4bS76tBBDE="),
  new OperationData.Builder()
  .withClientMethodDescriptor("(Ljava/lang/Long;)Lcom/google/web/bindery/requestfactory/shared/Request;")
  .withDomainMethodDescriptor("(Ljava/lang/Long;)Ledu/columbia/sportsleagueandroid/server/Bowler;")
  .withMethodName("readBowler")
  .withRequestContext("edu.columbia.sportsleagueandroid.shared.SportsLeagueAndroidRequest")
  .build());
withOperation(new OperationKey("Trb4ilYAR2YOla8AfT1DyonCGq4="),
  new OperationData.Builder()
  .withClientMethodDescriptor("(Ljava/lang/Long;)Lcom/google/web/bindery/requestfactory/shared/Request;")
  .withDomainMethodDescriptor("(Ljava/lang/Long;)Ledu/columbia/sportsleagueandroid/server/News;")
  .withMethodName("readNews")
  .withRequestContext("edu.columbia.sportsleagueandroid.shared.SportsLeagueAndroidRequest")
  .build());
withOperation(new OperationKey("wdCfAqEuov4X8$b1yF4qvX6p494="),
  new OperationData.Builder()
  .withClientMethodDescriptor("(Ljava/lang/String;ILjava/lang/String;)Lcom/google/web/bindery/requestfactory/shared/Request;")
  .withDomainMethodDescriptor("(Ljava/lang/String;ILjava/lang/String;)Ledu/columbia/sportsleagueandroid/server/Batsman;")
  .withMethodName("updateBatsman")
  .withRequestContext("edu.columbia.sportsleagueandroid.shared.SportsLeagueAndroidRequest")
  .build());
withOperation(new OperationKey("JvqIMFqKeKuqKIR0j0UU3nsRkT4="),
  new OperationData.Builder()
  .withClientMethodDescriptor("(Ledu/columbia/sportsleagueandroid/shared/NewsProxy;)Lcom/google/web/bindery/requestfactory/shared/Request;")
  .withDomainMethodDescriptor("(Ledu/columbia/sportsleagueandroid/server/News;)V")
  .withMethodName("deleteNews")
  .withRequestContext("edu.columbia.sportsleagueandroid.shared.SportsLeagueAndroidRequest")
  .build());
withOperation(new OperationKey("1FsY0Ypkl0WUZ4oZ69y5FEeY0eg="),
  new OperationData.Builder()
  .withClientMethodDescriptor("()Lcom/google/web/bindery/requestfactory/shared/Request;")
  .withDomainMethodDescriptor("()Ledu/columbia/sportsleagueandroid/server/Batsman;")
  .withMethodName("createBatsman")
  .withRequestContext("edu.columbia.sportsleagueandroid.shared.SportsLeagueAndroidRequest")
  .build());
withOperation(new OperationKey("jZy9Lisr987SI8T0vf7N28QyH9o="),
  new OperationData.Builder()
  .withClientMethodDescriptor("(Ledu/columbia/sportsleagueandroid/shared/MasterScorecardProxy;)Lcom/google/web/bindery/requestfactory/shared/Request;")
  .withDomainMethodDescriptor("(Ledu/columbia/sportsleagueandroid/server/MasterScorecard;)V")
  .withMethodName("deleteMasterScorecard")
  .withRequestContext("edu.columbia.sportsleagueandroid.shared.SportsLeagueAndroidRequest")
  .build());
withOperation(new OperationKey("Vhjssb1B64ykR1hzdN3GqPYEgFI="),
  new OperationData.Builder()
  .withClientMethodDescriptor("(Ledu/columbia/sportsleagueandroid/shared/BatsmanProxy;)Lcom/google/web/bindery/requestfactory/shared/Request;")
  .withDomainMethodDescriptor("(Ledu/columbia/sportsleagueandroid/server/Batsman;)V")
  .withMethodName("deleteBatsman")
  .withRequestContext("edu.columbia.sportsleagueandroid.shared.SportsLeagueAndroidRequest")
  .build());
withOperation(new OperationKey("PtYNw3PNnE2aCIN7r6VjyrWPr$Y="),
  new OperationData.Builder()
  .withClientMethodDescriptor("(IILjava/lang/String;Ljava/lang/String;IIDLjava/lang/String;Ljava/lang/String;)Lcom/google/web/bindery/requestfactory/shared/Request;")
  .withDomainMethodDescriptor("(IILjava/lang/String;Ljava/lang/String;IIDLjava/lang/String;Ljava/lang/String;)Ljava/util/List;")
  .withMethodName("queryMasterScorecards")
  .withRequestContext("edu.columbia.sportsleagueandroid.shared.SportsLeagueAndroidRequest")
  .build());
withOperation(new OperationKey("HQC09_ZrZsgO_3LurijI1sUoH7o="),
  new OperationData.Builder()
  .withClientMethodDescriptor("()Lcom/google/web/bindery/requestfactory/shared/Request;")
  .withDomainMethodDescriptor("()Ledu/columbia/sportsleagueandroid/server/Player;")
  .withMethodName("createPlayer")
  .withRequestContext("edu.columbia.sportsleagueandroid.shared.SportsLeagueAndroidRequest")
  .build());
withOperation(new OperationKey("IzhR5p97NjfPl38tnzP_9TqeVQA="),
  new OperationData.Builder()
  .withClientMethodDescriptor("(Ledu/columbia/sportsleagueandroid/shared/PlayerProxy;)Lcom/google/web/bindery/requestfactory/shared/Request;")
  .withDomainMethodDescriptor("(Ledu/columbia/sportsleagueandroid/server/Player;)Ledu/columbia/sportsleagueandroid/server/Player;")
  .withMethodName("updatePlayer")
  .withRequestContext("edu.columbia.sportsleagueandroid.shared.SportsLeagueAndroidRequest")
  .build());
withOperation(new OperationKey("_9OUK4WklD59UPPGnrMkju8TWlA="),
  new OperationData.Builder()
  .withClientMethodDescriptor("(Ledu/columbia/sportsleagueandroid/shared/MatchOverProxy;)Lcom/google/web/bindery/requestfactory/shared/Request;")
  .withDomainMethodDescriptor("(Ledu/columbia/sportsleagueandroid/server/MatchOver;)V")
  .withMethodName("deleteMatchOver")
  .withRequestContext("edu.columbia.sportsleagueandroid.shared.SportsLeagueAndroidRequest")
  .build());
withOperation(new OperationKey("nLnRpjueO1mU85vIjGUZWsrrXMc="),
  new OperationData.Builder()
  .withClientMethodDescriptor("(Ledu/columbia/sportsleagueandroid/shared/TeamProxy;)Lcom/google/web/bindery/requestfactory/shared/Request;")
  .withDomainMethodDescriptor("(Ledu/columbia/sportsleagueandroid/server/Team;)Ledu/columbia/sportsleagueandroid/server/Team;")
  .withMethodName("updateTeam")
  .withRequestContext("edu.columbia.sportsleagueandroid.shared.SportsLeagueAndroidRequest")
  .build());
withOperation(new OperationKey("1LCxau55uHwkvHfHXeClXeE1Pyo="),
  new OperationData.Builder()
  .withClientMethodDescriptor("(Ledu/columbia/sportsleagueandroid/shared/MatchInningProxy;)Lcom/google/web/bindery/requestfactory/shared/Request;")
  .withDomainMethodDescriptor("(Ledu/columbia/sportsleagueandroid/server/MatchInning;)V")
  .withMethodName("deleteMatchInning")
  .withRequestContext("edu.columbia.sportsleagueandroid.shared.SportsLeagueAndroidRequest")
  .build());
withOperation(new OperationKey("KYvz4tfPMRs4ZXzEXHGZbixcc0M="),
  new OperationData.Builder()
  .withClientMethodDescriptor("(Ljava/lang/Long;)Lcom/google/web/bindery/requestfactory/shared/Request;")
  .withDomainMethodDescriptor("(Ljava/lang/Long;)Ledu/columbia/sportsleagueandroid/server/MatchInning;")
  .withMethodName("readMatchInning")
  .withRequestContext("edu.columbia.sportsleagueandroid.shared.SportsLeagueAndroidRequest")
  .build());
withOperation(new OperationKey("VB0okp245_pgTGN$$y3P$dOs52Q="),
  new OperationData.Builder()
  .withClientMethodDescriptor("(Ljava/lang/String;)Lcom/google/web/bindery/requestfactory/shared/Request;")
  .withDomainMethodDescriptor("(Ljava/lang/String;)Ljava/util/List;")
  .withMethodName("queryMatchSchedules")
  .withRequestContext("edu.columbia.sportsleagueandroid.shared.SportsLeagueAndroidRequest")
  .build());
withOperation(new OperationKey("f3nE3WfyTcd9CBCO2tHhKWnAzCc="),
  new OperationData.Builder()
  .withClientMethodDescriptor("()Lcom/google/web/bindery/requestfactory/shared/Request;")
  .withDomainMethodDescriptor("()Ljava/util/List;")
  .withMethodName("queryPlayers")
  .withRequestContext("edu.columbia.sportsleagueandroid.shared.SportsLeagueAndroidRequest")
  .build());
withOperation(new OperationKey("h1K6E3L0AR5$$k2DLzLKFk5FHTU="),
  new OperationData.Builder()
  .withClientMethodDescriptor("(Ledu/columbia/sportsleagueandroid/shared/StadiumProxy;)Lcom/google/web/bindery/requestfactory/shared/Request;")
  .withDomainMethodDescriptor("(Ledu/columbia/sportsleagueandroid/server/Stadium;)Ledu/columbia/sportsleagueandroid/server/Stadium;")
  .withMethodName("updateStadium")
  .withRequestContext("edu.columbia.sportsleagueandroid.shared.SportsLeagueAndroidRequest")
  .build());
withOperation(new OperationKey("Qt$PzR7kENiCavInimLOy5$n9b0="),
  new OperationData.Builder()
  .withClientMethodDescriptor("(Ljava/lang/Long;)Lcom/google/web/bindery/requestfactory/shared/Request;")
  .withDomainMethodDescriptor("(Ljava/lang/Long;)Ledu/columbia/sportsleagueandroid/server/MasterScorecard;")
  .withMethodName("readMasterScorecard")
  .withRequestContext("edu.columbia.sportsleagueandroid.shared.SportsLeagueAndroidRequest")
  .build());
withOperation(new OperationKey("OxIOlobWq4RzslIKjNjJlfYUMDs="),
  new OperationData.Builder()
  .withClientMethodDescriptor("(Ljava/lang/Long;)Lcom/google/web/bindery/requestfactory/shared/Request;")
  .withDomainMethodDescriptor("(Ljava/lang/Long;)Ledu/columbia/sportsleagueandroid/server/Stadium;")
  .withMethodName("readStadium")
  .withRequestContext("edu.columbia.sportsleagueandroid.shared.SportsLeagueAndroidRequest")
  .build());
withOperation(new OperationKey("jJiB0P6nt2Z8T37iimQrNAM0GNo="),
  new OperationData.Builder()
  .withClientMethodDescriptor("()Lcom/google/web/bindery/requestfactory/shared/InstanceRequest;")
  .withDomainMethodDescriptor("()Ljava/lang/String;")
  .withMethodName("send")
  .withRequestContext("edu.columbia.sportsleagueandroid.client.MyRequestFactory$MessageRequest")
  .build());
withOperation(new OperationKey("D550wYO5st8vTIjOoZnKot9s81g="),
  new OperationData.Builder()
  .withClientMethodDescriptor("()Lcom/google/web/bindery/requestfactory/shared/Request;")
  .withDomainMethodDescriptor("()Ljava/lang/String;")
  .withMethodName("getMessage")
  .withRequestContext("edu.columbia.sportsleagueandroid.client.MyRequestFactory$HelloWorldRequest")
  .build());
withOperation(new OperationKey("Te7ODP7aZ6hfPib_ew7mfDBjnAE="),
  new OperationData.Builder()
  .withClientMethodDescriptor("()Lcom/google/web/bindery/requestfactory/shared/InstanceRequest;")
  .withDomainMethodDescriptor("()V")
  .withMethodName("register")
  .withRequestContext("edu.columbia.sportsleagueandroid.client.MyRequestFactory$RegistrationInfoRequest")
  .build());
withOperation(new OperationKey("YqW9I5fdriQ2WkBamSRi7gnD_C8="),
  new OperationData.Builder()
  .withClientMethodDescriptor("()Lcom/google/web/bindery/requestfactory/shared/InstanceRequest;")
  .withDomainMethodDescriptor("()V")
  .withMethodName("unregister")
  .withRequestContext("edu.columbia.sportsleagueandroid.client.MyRequestFactory$RegistrationInfoRequest")
  .build());
withRawTypeToken("3HVlCDi2C_VK$X14oFgUbm9TVJI=", "edu.columbia.sportsleagueandroid.shared.BatsmanProxy");
withRawTypeToken("awKnik$bFbSuAaQZ32Jzdu0dPPg=", "edu.columbia.sportsleagueandroid.shared.BowlerProxy");
withRawTypeToken("ioyhtdeYChaI64StldbnwabLq60=", "edu.columbia.sportsleagueandroid.shared.MasterScorecardProxy");
withRawTypeToken("eQ9Y76UU8OqjsAEdl6Uc0lPXcok=", "edu.columbia.sportsleagueandroid.shared.MatchInningProxy");
withRawTypeToken("IiEuKI$_ALyOsb$xAwgZfhpI4Us=", "edu.columbia.sportsleagueandroid.shared.MatchOverProxy");
withRawTypeToken("DBR3$QrYNa7tO2JwG50UxP15qi0=", "edu.columbia.sportsleagueandroid.shared.MatchScheduleProxy");
withRawTypeToken("jX4WekfZZynciImPq89vbLh5iCE=", "edu.columbia.sportsleagueandroid.shared.MessageProxy");
withRawTypeToken("TcoSUTcwxzu4bsAFz9MMfQ56KW8=", "edu.columbia.sportsleagueandroid.shared.NewsProxy");
withRawTypeToken("zzckLhT2K3Rw6bI4cz2azIdlMIk=", "edu.columbia.sportsleagueandroid.shared.PlayerProxy");
withRawTypeToken("7BtNwpkS6O4VevRcu1K4V8qz7z8=", "edu.columbia.sportsleagueandroid.shared.RegistrationInfoProxy");
withRawTypeToken("HXOYp5qDQXzR0AE6LnlAyX3izxg=", "edu.columbia.sportsleagueandroid.shared.StadiumProxy");
withRawTypeToken("0qhpRo9E8PEFNqaYl6pbM7fus0g=", "edu.columbia.sportsleagueandroid.shared.TeamProxy");
withRawTypeToken("8KVVbwaaAtl6KgQNlOTsLCp9TIU=", "com.google.web.bindery.requestfactory.shared.ValueProxy");
withRawTypeToken("FXHD5YU0TiUl3uBaepdkYaowx9k=", "com.google.web.bindery.requestfactory.shared.BaseProxy");
withClientToDomainMappings("edu.columbia.sportsleagueandroid.server.Batsman", Arrays.asList("edu.columbia.sportsleagueandroid.shared.BatsmanProxy"));
withClientToDomainMappings("edu.columbia.sportsleagueandroid.server.Bowler", Arrays.asList("edu.columbia.sportsleagueandroid.shared.BowlerProxy"));
withClientToDomainMappings("edu.columbia.sportsleagueandroid.server.MasterScorecard", Arrays.asList("edu.columbia.sportsleagueandroid.shared.MasterScorecardProxy"));
withClientToDomainMappings("edu.columbia.sportsleagueandroid.server.MatchInning", Arrays.asList("edu.columbia.sportsleagueandroid.shared.MatchInningProxy"));
withClientToDomainMappings("edu.columbia.sportsleagueandroid.server.MatchOver", Arrays.asList("edu.columbia.sportsleagueandroid.shared.MatchOverProxy"));
withClientToDomainMappings("edu.columbia.sportsleagueandroid.server.MatchSchedule", Arrays.asList("edu.columbia.sportsleagueandroid.shared.MatchScheduleProxy"));
withClientToDomainMappings("edu.columbia.sportsleagueandroid.server.Message", Arrays.asList("edu.columbia.sportsleagueandroid.shared.MessageProxy"));
withClientToDomainMappings("edu.columbia.sportsleagueandroid.server.News", Arrays.asList("edu.columbia.sportsleagueandroid.shared.NewsProxy"));
withClientToDomainMappings("edu.columbia.sportsleagueandroid.server.Player", Arrays.asList("edu.columbia.sportsleagueandroid.shared.PlayerProxy"));
withClientToDomainMappings("edu.columbia.sportsleagueandroid.server.RegistrationInfo", Arrays.asList("edu.columbia.sportsleagueandroid.shared.RegistrationInfoProxy"));
withClientToDomainMappings("edu.columbia.sportsleagueandroid.server.Stadium", Arrays.asList("edu.columbia.sportsleagueandroid.shared.StadiumProxy"));
withClientToDomainMappings("edu.columbia.sportsleagueandroid.server.Team", Arrays.asList("edu.columbia.sportsleagueandroid.shared.TeamProxy"));
}}
