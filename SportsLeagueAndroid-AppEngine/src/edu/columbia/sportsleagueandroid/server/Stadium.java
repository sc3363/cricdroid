package edu.columbia.sportsleagueandroid.server;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Stadium 
{
	@Id
	private String stadiumName;
	private String stadiumCity;
	private String stadiumCountry;
	private long stadiumCapacity;
	private double latitude;
	private double longitude;
	
	public Stadium()
	{}
	
	public String getStadiumName() 
	{
		return stadiumName;
	}
	
	public void setStadiumName(String stadiumName)
	{
		this.stadiumName = stadiumName;
	}
	
	public String getStadiumCity()
	{
		return stadiumCity;
	}
	
	public void setStadiumCity(String stadiumCity)
	{
		this.stadiumCity = stadiumCity;
	}
	
	public String getStadiumCountry() 
	{
		return stadiumCountry;
	}
	
	public void setStadiumCountry(String stadiumCountry) 
	{
		this.stadiumCountry = stadiumCountry;
	}
	
	public long getStadiumCapacity() 
	{
		return stadiumCapacity;
	}
	
	public void setStadiumCapacity(long stadiumCapacity)
	{
		this.stadiumCapacity = stadiumCapacity;
	}
	
	@Override
	public String toString()
	{
		StringBuilder b = new StringBuilder();
		b.append("Capacity");
		b.append(stadiumCapacity);
		b.append(stadiumCity);
		b.append(stadiumCountry);
		b.append("Name");
		b.append(stadiumName);
		return b.toString();
		
	}

	public double getLatitude() 
	{
		return latitude;
	}

	public void setLatitude(double latitude) 
	{
		this.latitude = latitude;
	}

	public double getLongitude() 
	{
		return longitude;
	}

	public void setLongitude(double longitude)
	{
		this.longitude = longitude;
	}
}
