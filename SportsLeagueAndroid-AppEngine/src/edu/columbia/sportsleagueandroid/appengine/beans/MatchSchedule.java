package edu.columbia.sportsleagueandroid.appengine.beans;

public class MatchSchedule 
{
	private int matchNumber;
	private String teamOne;
	private String teamTwo;
	private String matchType;
	private String stadiumName;
	private String stadiumCity;
	private String matchDate;
	private String matchTiming;
	private String matchResult;
	private long matchAttendance;
	
	public int getMatchNumber() 
	{
		return matchNumber;
	}
	
	public void setMatchNumber(int matchNumber) 
	{
		this.matchNumber = matchNumber;
	}
	
	public String getTeamOne() 
	{
		return teamOne;
	}
	
	public void setTeamOne(String teamOne) 
	{
		this.teamOne = teamOne;
	}
	
	public String getTeamTwo() 
	{
		return teamTwo;
	}
	
	public void setTeamTwo(String teamTwo) 
	{
		this.teamTwo = teamTwo;
	}
	
	public String getMatchType() 
	{
		return matchType;
	}
	
	public void setMatchType(String matchType) 
	{
		this.matchType = matchType;
	}
	
	public String getMatchDate() 
	{
		return matchDate;
	}
	
	public void setStadiumName(String stadiumName)
	{
		this.stadiumName = stadiumName;
	}
	
	public String getStadiumName()
	{
		return stadiumName;
	}
	
	public void setStadiumCity(String stadiumCity)
	{
		this.stadiumCity = stadiumCity;
	}
	
	public String setStadiumCity()
	{
		return stadiumCity;
	}
	
	public void setMatchDate(String matchDate) 
	{
		this.matchDate = matchDate;
	}
	
	public String getMatchTiming() 
	{
		return matchTiming;
	}
	
	public void setMatchTiming(String matchTiming) 
	{
		this.matchTiming = matchTiming;
	}
	
	public String getMatchResult()
	{
		return matchResult;
	}
	
	public void setMatchResult(String matchResult) 
	{
		this.matchResult = matchResult;
	}
	
	public long getMatchAttendance()
	{
		return matchAttendance;
	}
	
	public void setMatchAttendance(long matchAttendance)
	{
		this.matchAttendance = matchAttendance;
	}
}
