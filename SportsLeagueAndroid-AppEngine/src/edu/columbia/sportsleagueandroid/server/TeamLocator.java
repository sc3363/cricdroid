package edu.columbia.sportsleagueandroid.server;

import com.google.web.bindery.requestfactory.shared.Locator;


public class TeamLocator extends Locator<Team, Void> {

	@Override
	public Team create(Class<? extends Team> clazz) {
		return new Team();
	}

	@Override
	public Team find(Class<? extends Team> clazz, Void id) {
		return create(clazz);
	}

	@Override
	public Class<Team> getDomainType() {
		return Team.class;
	}

	@Override
	public Void getId(Team domainObject) {
		return null;
	}

	@Override
	public Class<Void> getIdType() {
		return Void.class;
	}

	@Override
	public Object getVersion(Team domainObject) {
		return null;
	}

}
