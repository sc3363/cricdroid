package edu.columbia.sportsleagueandroid;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;

import com.google.web.bindery.requestfactory.shared.Receiver;
import com.google.web.bindery.requestfactory.shared.ServerFailure;

import edu.columbia.sportsleagueandroid.client.MyRequestFactory;
import edu.columbia.sportsleagueandroid.shared.NewsProxy;

import android.app.ListActivity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListView;

public class DetailedNewsActivity extends ListActivity 
{
	Connection dbConnection;
	@Override
	public void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
	}
	
	private void setScreenContent()
	{
		setContentView(R.layout.nestedlive_layout);
		final String extraString = getIntent().getExtras().getString("extra");
		
		final List<NewsProxy> newsFeed = new ArrayList<NewsProxy>();
		
		// Use an AsyncTask to avoid blocking the UI thread
		new AsyncTask<Void, Void, List<NewsProxy>>() 
		{
			@Override
			protected List<NewsProxy> doInBackground(Void... arg0) 
			{
				MyRequestFactory requestFactory = Util.getRequestFactory(getBaseContext(), MyRequestFactory.class);
				
				requestFactory.sportsLeagueAndroidRequest().queryNewss(extraString).fire(new Receiver<List<NewsProxy>>() 
				{
					@Override
					public void onFailure(ServerFailure error)
					{
						String message = "Failure: "+ error.getMessage();
					}

					@Override
					public void onSuccess(List<NewsProxy> result) 
					{
						newsFeed.addAll(result);
					}
				});
				return newsFeed;
			}

			@Override
			protected void onPostExecute(List<NewsProxy> newsFeed) 
			{
				int storyId = newsFeed.get(0).getNewsId();
				String headline = newsFeed.get(0).getNewsHeadline();
				String story = newsFeed.get(0).getNewsContent();
				String date = newsFeed.get(0).getNewsTime();
				
				String[] dateString = date.split("-");
				
				if(dateString[1].equalsIgnoreCase("01"))
				{
					dateString[1] = "January";
				}
				else if(dateString[1].equalsIgnoreCase("02"))
				{
					dateString[1] = "February";
				}
				else if(dateString[1].equalsIgnoreCase("03"))
				{
					dateString[1] = "March";
				}
				else if(dateString[1].equalsIgnoreCase("04"))
				{
					dateString[1] = "April";
				}
				else if(dateString[1].equalsIgnoreCase("05"))
				{
					dateString[1] = "May";
				}
				else if(dateString[1].equalsIgnoreCase("06"))
				{
					dateString[1] = "June";
				}
				else if(dateString[1].equalsIgnoreCase("07"))
				{
					dateString[1] = "July";
				}
				else if(dateString[1].equalsIgnoreCase("08"))
				{
					dateString[1] = "August";
				}
				else if(dateString[1].equalsIgnoreCase("09"))
				{
					dateString[1] = "September";
				}
				else if(dateString[1].equalsIgnoreCase("10"))
				{
					dateString[1] = "October";
				}
				else if(dateString[1].equalsIgnoreCase("11"))
				{
					dateString[1] = "November";
				}
				else if(dateString[1].equalsIgnoreCase("12"))
				{
					dateString[1]  = "December";
				}
				
				String[] news = new String[3];
				news[0] = "Story# "+storyId+" | "+dateString[1]+" "+dateString[2]+", "+dateString[0];
				news[1] = headline;
				news[2] = story;
				
				ArrayAdapter<String> adapter = new ArrayAdapter<String>(getBaseContext(), R.layout.news_list_item, news);
				
				setListAdapter(adapter);
				adapter.notifyDataSetChanged();
				ListView lv = getListView();
				lv.setTextFilterEnabled(true);
				
			}
		}.execute();
	}

	@Override
	public void onResume() 
	{
		//onDestroy();
		super.onResume();
		
		setScreenContent();
		setContentView(R.layout.nestedlive_layout);
	}
	
	 @Override
	 public void onDestroy()
	 {
	     super.onDestroy();
	 }
}