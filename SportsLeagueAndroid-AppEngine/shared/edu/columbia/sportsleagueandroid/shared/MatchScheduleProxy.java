package edu.columbia.sportsleagueandroid.shared;

import com.google.web.bindery.requestfactory.shared.ProxyForName;
import com.google.web.bindery.requestfactory.shared.ValueProxy;

@ProxyForName(value = "edu.columbia.sportsleagueandroid.server.MatchSchedule", locator = "edu.columbia.sportsleagueandroid.server.MatchScheduleLocator")
public interface MatchScheduleProxy extends ValueProxy {

	int getMatchNumber();

	void setMatchNumber(int matchNumber);

	String getTeamOne();

	void setTeamOne(String teamOne);

	String getTeamTwo();

	void setTeamTwo(String teamTwo);

	String getMatchType();

	void setMatchType(String matchType);

	String getMatchDate();

	void setStadiumName(String stadiumName);

	String getStadiumName();

	void setStadiumCity(String stadiumCity);

	String getStadiumCity();

	void setMatchDate(String matchDate);

	String getMatchTiming();

	void setMatchTiming(String matchTiming);

	String getMatchResult();

	void setMatchResult(String matchResult);

	long getMatchAttendance();

	void setMatchAttendance(long matchAttendance);

}
