package edu.columbia.sportsleagueandroid.server;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class MatchOver 
{
	private int matchNumber;
	private int inningNumber;
	private int overNumber;
	@Id
	private int ballNumber;
	private String ballTime;
	private String action;
	private String batsman;
	private String nonStriker;
	private String bowler;
	private String comment;
	private int runsScored;
	
	public MatchOver()
	{}
	
	public int getMatchNumber() 
	{
		return matchNumber;
	}
	
	public void setMatchNumber(int matchNumber) 
	{
		this.matchNumber = matchNumber;
	}
	
	public int getInningNumber()
	{
		return inningNumber;
	}
	
	public void setInningNumber(int inningNumber) 
	{
		this.inningNumber = inningNumber;
	}
	
	public int getOverNumber() 
	{
		return overNumber;
	}
	
	public void setOverNumber(int overNumber) 
	{
		this.overNumber = overNumber;
	}
	
	public int getBallNumber() 
	{
		return ballNumber;
	}
	
	public void setBallNumber(int ballNumber) 
	{
		this.ballNumber = ballNumber;
	}
	
	public String getBallTime() 
	{
		return ballTime;
	}
	
	public void setBallTime(String ballTime) 
	{
		this.ballTime = ballTime;
	}
	
	public String getAction() 
	{
		return action;
	}
	
	public void setAction(String action)
	{
		this.action = action;
	}
	
	public String getBatsman()
	{
		return batsman;
	}
	
	public void setBatsman(String batsman) 
	{
		this.batsman = batsman;
	}
	
	public String getNonStriker()
	{
		return nonStriker;
	}
	
	public void setNonStriker(String nonStriker)
	{
		this.nonStriker = nonStriker;
	}
	
	public String getBowler() 
	{
		return bowler;
	}
	
	public void setBowler(String bowler)
	{
		this.bowler = bowler;
	}
	
	public String getComment() 
	{
		return comment;
	}
	
	public void setComment(String comment)
	{
		this.comment = comment;
	}
	
	public int getRunsScored() 
	{
		return runsScored;
	}
	
	public void setRunsScored(int runsScored) 
	{
		this.runsScored = runsScored;
	}
}
