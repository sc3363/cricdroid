package edu.columbia.sportsleagueandroid;

import java.util.ArrayList;
import java.util.List;

import com.google.web.bindery.requestfactory.shared.Receiver;
import com.google.web.bindery.requestfactory.shared.ServerFailure;

import edu.columbia.sportsleagueandroid.client.MyRequestFactory;
import edu.columbia.sportsleagueandroid.shared.MasterScorecardProxy;
import android.app.ListActivity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Toast;

public class FullScorecardActivity extends ListActivity 
{
	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
	}
	
	private void setScreenContent()
	{
		String matchData = getIntent().getExtras().getString("match_data");
		String inningData = getIntent().getExtras().getString("inning_data");
		
		String[] segregatedInningData = inningData.split("[ ]");
		String teamScoreData[] = segregatedInningData[1].split("/");
		
		int teamScore = Integer.parseInt(teamScoreData[0]);
		int teamWickets = Integer.parseInt(teamScoreData[1]);
		
		double teamOvers = Double.parseDouble(segregatedInningData[6]);
		
		int inningNumber = getIntent().getExtras().getInt("inningNumber");
		String [] matchStarterData = matchData.split("\n");
		
		String match[] = matchStarterData[0].split("#");
		int matchNumber = Integer.parseInt(match[1].trim());
		
		String teams[] = matchStarterData[1].trim().split(" vs ");
		String teamOne = teams[0];
		String teamTwo = teams[1];
		String stadiumResidual[] = matchStarterData[2].split(", ");
		String stadiumName = stadiumResidual[0];
		String stadiumCity = stadiumResidual[1];
		
		String battingTeam, fieldingTeam;
		if(inningData.contains(teamOne))
		{
			battingTeam = teamOne;
			fieldingTeam = teamTwo;
		}
		else
		{
			battingTeam = teamTwo;
			fieldingTeam = teamOne;
		}
		
		setMatchDetails(teamScore, teamWickets, teamOvers, teamOne, teamTwo, matchNumber, inningNumber, battingTeam, fieldingTeam, stadiumName, stadiumCity);
	}

	private void setMatchDetails(final int teamScore, final int teamWickets, final double teamOvers, final String teamOne, final String teamTwo, final int matchNumber, 
			final int inningNumber, final String battingTeam, final String fieldingTeam, final String stadiumName,final String stadiumCity)
	{
		//setContentView(R.layout.nestedlive_layout);
		final List<MasterScorecardProxy> masterScorecard = new ArrayList<MasterScorecardProxy>();
		
		
		// Use an AsyncTask to avoid blocking the UI thread
		new AsyncTask<Void, Void, List<MasterScorecardProxy>>() 
		{
			@Override
			protected List<MasterScorecardProxy> doInBackground(Void... arg0) 
			{
				MyRequestFactory requestFactory = Util.getRequestFactory(getBaseContext(), MyRequestFactory.class);
						
				requestFactory.sportsLeagueAndroidRequest().queryMasterScorecards(matchNumber, inningNumber, battingTeam, fieldingTeam, teamScore, teamWickets, 
						teamOvers, stadiumName, stadiumCity)
				.fire(new Receiver<List<MasterScorecardProxy>>() 
				{
					@Override
					public void onFailure(ServerFailure error)
					{
						String message = "Failure: "+ error.getMessage();
					}

					@Override
					public void onSuccess(List<MasterScorecardProxy> result) 
					{
						masterScorecard.addAll(result);
					}
				});
				
				return masterScorecard;
			}

			@Override
			protected void onPostExecute(final List<MasterScorecardProxy> masterScorecard) 
			{
				String[] passString; 
				
				String matchDetailsString = "Match# "+matchNumber+"\n"+
						""+battingTeam+" vs "+teamTwo+"\n"+
						"Live from "+stadiumName+", "+stadiumCity+"\n";
				
				if(masterScorecard == null || masterScorecard.size() == 0)
				{
					passString = new String[8];
					passString[0] = matchDetailsString;
					passString[1] = "";
					passString[2] = "";
					passString[3] = "The match telecast is yet to begin";
					passString[4] = "";
					passString[5] = "";
					passString[6] = "";
				}
				else
				{
					passString = new String[masterScorecard.get(0).getBowler().size()+11+4];
					passString[0] = matchDetailsString;
					passString[1] = "Inning# "+inningNumber+" | "+battingTeam+" "+teamScore+"/"+teamWickets+"    Overs: "+teamOvers+"\n";
					for(int batsmanCount = 0; batsmanCount <=10; batsmanCount++)
					{
						if(masterScorecard.get(0).getBatsman().get(batsmanCount).getStatus().equalsIgnoreCase("NO"))
						{
							passString[batsmanCount+2] = masterScorecard.get(0).getBatsman().get(batsmanCount).getBatsmanName()+"*";
						}
						else
						{
							passString[batsmanCount+2] = masterScorecard.get(0).getBatsman().get(batsmanCount).getBatsmanName();
						}
						
						passString[batsmanCount+2]+= " | "+masterScorecard.get(0).getBatsman().get(batsmanCount).getRunsScored()+" | "+
								masterScorecard.get(0).getBatsman().get(batsmanCount).getBallsPlayed()+" | "+
								masterScorecard.get(0).getBatsman().get(batsmanCount).getFours()+" | "+
								masterScorecard.get(0).getBatsman().get(batsmanCount).getSixes();
					}
					
					passString[13] = "";
					passString[14] = fieldingTeam+"'s Bowling Scorecard";
					for(int bowlerCount = 0; bowlerCount < masterScorecard.get(0).getBowler().size(); bowlerCount++)
					{
						passString[bowlerCount+15] = masterScorecard.get(0).getBowler().get(bowlerCount).getBowlerName()+" | "+
								masterScorecard.get(0).getBowler().get(bowlerCount).getOversBowled()+" | "+masterScorecard.get(0).getBowler().get(bowlerCount)
								.getRunsConceded()+" | "+masterScorecard.get(0).getBowler().get(bowlerCount).getBowlerWickets();
					}
				}

			    final ArrayAdapter<String> adapter = new ArrayAdapter<String>(getBaseContext(), R.layout.scorecard_listitem, passString);
			    
			    setListAdapter(adapter);
				adapter.notifyDataSetChanged();
				ListView lv = getListView();
				lv.setTextFilterEnabled(true);
				
				lv.setOnItemClickListener(new OnItemClickListener()
				{
					public void onItemClick(AdapterView<?> lv, View view,int position, long id)
					{	
						if(position == 0 || position == 1)
						{
							Toast.makeText(getApplicationContext(), lv.getItemAtPosition(position).toString(), Toast.LENGTH_LONG).show();
						}
						else if(position > 1 && position < 13)
						{
							String[] toastText = lv.getItemAtPosition(position).toString().split("[\\|]");
							String batsman = toastText[0].trim();
							int runs = Integer.parseInt(toastText[1].trim());
							int balls = Integer.parseInt(toastText[2].trim());
							int fours = Integer.parseInt(toastText[3].trim());
							int sixes = Integer.parseInt(toastText[4].trim());
							
							Toast.makeText(getApplicationContext(), batsman+" | "+runs+" runs | "+balls+" balls | "+fours+" fours | "+sixes+" sixes", 
									Toast.LENGTH_LONG).show();
						}
						else if(position > 14)
						{
							String[] toastText = lv.getItemAtPosition(position).toString().split("[\\|]");
							String bowler = toastText[0].trim();
							double overs = Double.parseDouble(toastText[1].trim());
							int runs = Integer.parseInt(toastText[2].trim());
							int wickets = Integer.parseInt(toastText[3].trim());
							
							Toast.makeText(getApplicationContext(), bowler+" | "+overs+" overs | "+runs+" runs | "+wickets+" wickets", Toast.LENGTH_LONG).show();
						}
					}
				});
			}
		}.execute();
	}
	
	@Override
	public void onResume() 
	{
		super.onResume();
		
		setScreenContent();
		//setContentView(R.layout.nestedlive_layout);
	}
	
//	@Override
//	public void onBackPressed() 
//	{
//		// TODO Auto-generated method stub
//		super.onBackPressed();
//		Intent myIntent = new Intent(getParent(), FullScorecardActivity.class);
//		myIntent.putExtra("match_data", lv.getItemAtPosition(0).toString());
//		myIntent.putExtra("inning_data", lv.getItemAtPosition(position).toString());
//		myIntent.putExtra("inningNumber", position);
//		MatchScoreActivityGroup parentActivity = (MatchScoreActivityGroup)getParent();
//		parentActivity.startChildActivity("MatchScoreActivityGroup", myIntent);
//	}
//	
}
