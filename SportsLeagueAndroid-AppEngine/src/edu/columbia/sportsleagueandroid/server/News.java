package edu.columbia.sportsleagueandroid.server;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class News 
{
	@Id
	private int newsId;
	private String newsHeadline;
	private String newsContent;
	private String newsTime;
	
	public News()
	{}

	public int getNewsId() 
	{
		return newsId;
	}

	public void setNewsId(int newsId) 
	{
		this.newsId = newsId;
	}

	public String getNewsHeadline() 
	{
		return newsHeadline;
	}

	public void setNewsHeadline(String newsHeadline) 
	{
		this.newsHeadline = newsHeadline;
	}

	public String getNewsContent() 
	{
		return newsContent;
	}

	public void setNewsContent(String newsContent) 
	{
		this.newsContent = newsContent;
	}

	public String getNewsTime() 
	{
		return newsTime;
	}

	public void setNewsTime(String newsTime) 
	{
		this.newsTime = newsTime;
	}
	
}
