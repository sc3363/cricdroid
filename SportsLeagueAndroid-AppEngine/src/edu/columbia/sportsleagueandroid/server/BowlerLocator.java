package edu.columbia.sportsleagueandroid.server;

import com.google.web.bindery.requestfactory.shared.Locator;


public class BowlerLocator extends Locator<Bowler, Void> {

	@Override
	public Bowler create(Class<? extends Bowler> clazz) {
		return new Bowler();
	}

	@Override
	public Bowler find(Class<? extends Bowler> clazz, Void id) {
		return create(clazz);
	}

	@Override
	public Class<Bowler> getDomainType() {
		return Bowler.class;
	}

	@Override
	public Void getId(Bowler domainObject) {
		return null;
	}

	@Override
	public Class<Void> getIdType() {
		return Void.class;
	}

	@Override
	public Object getVersion(Bowler domainObject) {
		return null;
	}

}
