package edu.columbia.sportsleagueandroid.server;

import com.google.web.bindery.requestfactory.shared.Locator;


public class MatchOverLocator extends Locator<MatchOver, Void> {

	@Override
	public MatchOver create(Class<? extends MatchOver> clazz) {
		return new MatchOver();
	}

	@Override
	public MatchOver find(Class<? extends MatchOver> clazz, Void id) {
		return create(clazz);
	}

	@Override
	public Class<MatchOver> getDomainType() {
		return MatchOver.class;
	}

	@Override
	public Void getId(MatchOver domainObject) {
		return null;
	}

	@Override
	public Class<Void> getIdType() {
		return Void.class;
	}

	@Override
	public Object getVersion(MatchOver domainObject) {
		return null;
	}

}
