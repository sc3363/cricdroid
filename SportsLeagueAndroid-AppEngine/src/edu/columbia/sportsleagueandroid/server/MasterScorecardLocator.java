package edu.columbia.sportsleagueandroid.server;

import com.google.web.bindery.requestfactory.shared.Locator;


public class MasterScorecardLocator extends Locator<MasterScorecard, Void> {

	@Override
	public MasterScorecard create(Class<? extends MasterScorecard> clazz) {
		return new MasterScorecard();
	}

	@Override
	public MasterScorecard find(Class<? extends MasterScorecard> clazz, Void id) {
		return create(clazz);
	}

	@Override
	public Class<MasterScorecard> getDomainType() {
		return MasterScorecard.class;
	}

	@Override
	public Void getId(MasterScorecard domainObject) {
		return null;
	}

	@Override
	public Class<Void> getIdType() {
		return Void.class;
	}

	@Override
	public Object getVersion(MasterScorecard domainObject) {
		return null;
	}

}
