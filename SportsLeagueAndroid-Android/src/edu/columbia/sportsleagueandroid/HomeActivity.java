package edu.columbia.sportsleagueandroid;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;

import com.google.web.bindery.requestfactory.shared.Receiver;
import com.google.web.bindery.requestfactory.shared.ServerFailure;

import edu.columbia.sportsleagueandroid.client.MyRequestFactory;
import edu.columbia.sportsleagueandroid.shared.NewsProxy;

import android.app.ListActivity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.AdapterView.OnItemClickListener;

public class HomeActivity extends ListActivity 
{
	Connection dbConnection;
	@Override
	public void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		setScreenContent();
	}
	
	private void setScreenContent()
	{
		setContentView(R.layout.home_layout);
		final List<NewsProxy> newsFeed = new ArrayList<NewsProxy>();
		
		// Use an AsyncTask to avoid blocking the UI thread
		new AsyncTask<Void, Void, List<NewsProxy>>() 
		{
			@Override
			protected List<NewsProxy> doInBackground(Void... arg0) 
			{
				MyRequestFactory requestFactory = Util.getRequestFactory(getBaseContext(), MyRequestFactory.class);
				
				requestFactory.sportsLeagueAndroidRequest().queryNewss("Headlines").fire(new Receiver<List<NewsProxy>>() 
				{
					@Override
					public void onFailure(ServerFailure error)
					{
						String message = "Failure: "+ error.getMessage();
					}

					@Override
					public void onSuccess(List<NewsProxy> result) 
					{
						newsFeed.addAll(result);
					}
				});
				return newsFeed;
			}

			@Override
			protected void onPostExecute(List<NewsProxy> newsFeed) 
			{
				String[] currentFeed = new String[newsFeed.size()];
				
				int counter = 0;
				for (NewsProxy newsProxy : newsFeed) 
				{
					StringBuilder sB = new StringBuilder();
					sB.append(newsProxy.getNewsHeadline());
					currentFeed[counter++] = sB.toString();
				}

				ArrayAdapter<String> adapter = new ArrayAdapter<String>(getBaseContext(), R.layout.list_item, currentFeed);
		
				setListAdapter(adapter);
				adapter.notifyDataSetChanged();
				ListView listView = getListView();
				listView.setTextFilterEnabled(true);

				listView.setOnItemClickListener(new OnItemClickListener()
				{
					public void onItemClick(AdapterView<?> listView, View view,int position, long id)
					{
						// When clicked, show a toast with the TextView text
						Intent myIntent = new Intent(getParent(), DetailedNewsActivity.class);
						myIntent.putExtra("extra", listView.getItemAtPosition(position).toString());
						NewsActivityGroup parentActivity = (NewsActivityGroup)getParent();
						parentActivity.startChildActivity("NewsActivityGroup", myIntent);
					}
				});

			}
		}.execute();
	}

	@Override
	public void onResume() 
	{
		//onDestroy();
		super.onResume();
		
		setScreenContent();
		setContentView(R.layout.home_layout);
	}
	
	 @Override
	 public void onDestroy()
	 {
	     super.onDestroy();
	 }
}