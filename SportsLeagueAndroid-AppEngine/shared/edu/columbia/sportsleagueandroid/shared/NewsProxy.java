package edu.columbia.sportsleagueandroid.shared;

import com.google.web.bindery.requestfactory.shared.ProxyForName;
import com.google.web.bindery.requestfactory.shared.ValueProxy;

@ProxyForName(value = "edu.columbia.sportsleagueandroid.server.News", locator = "edu.columbia.sportsleagueandroid.server.NewsLocator")
public interface NewsProxy extends ValueProxy {

	int getNewsId();

	void setNewsId(int newsId);

	String getNewsHeadline();

	void setNewsHeadline(String newsHeadline);

	String getNewsContent();

	void setNewsContent(String newsContent);

	String getNewsTime();

	void setNewsTime(String newsTime);

}
