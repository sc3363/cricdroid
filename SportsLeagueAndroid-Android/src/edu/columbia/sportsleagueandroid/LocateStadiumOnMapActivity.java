package edu.columbia.sportsleagueandroid;

import java.util.List;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.Window;

import com.google.android.maps.GeoPoint;
import com.google.android.maps.MapActivity;
import com.google.android.maps.MapController;
import com.google.android.maps.MapView;
import com.google.android.maps.Overlay;
import com.google.android.maps.OverlayItem;

public class LocateStadiumOnMapActivity extends MapActivity
{
	private static double[] latitude;
    private static double[] longitude;
    private static String[] stadiumName;
    private static String[] stadiumCity;
    private static String[] teamOne;
    private static String[] teamTwo;
    private static String[] matchDate;
    
    private MapController mapController;
    private MapView mapView;
    private GeoPoint geoPoint;
    String displayString = "";
    
    public static void feedLocation(String[] sName, String[] sCity, String[] tOne, String tTwo[], String mDate[], double[] lat, double[] longi)
    {
    	stadiumName = sName.clone();
    	stadiumCity = sCity.clone();
    	teamOne = tOne.clone();
    	teamTwo = tTwo.clone();
    	matchDate = mDate.clone();
    	latitude = lat.clone();
    	longitude = longi.clone();
    }
    
    public void onCreate(Bundle savedInstanceState) 
    {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);  
        setContentView(R.layout.map_layout);
        
        mapView = (MapView) findViewById(R.id.mapview);
        mapView.setSatellite(false);
        mapView.setTraffic(false);
        mapView.setBuiltInZoomControls(true);   
        mapController = mapView.getController();
        
        List<Overlay> mapOverlays = mapView.getOverlays();
        Drawable drawable = this.getResources().getDrawable(R.drawable.pin);
        DisplayOverlay displayOverlay = new DisplayOverlay(drawable, this);

        //Define a geo point on the map
        geoPoint = new GeoPoint((int) latitude[0],(int) longitude[0]);
        displayString = teamOne[0]+" vs "+teamTwo[0]+"\n"+stadiumName[0]+", "+stadiumCity[0];
        //Add the overlay
        OverlayItem overlayitem = new OverlayItem(geoPoint, matchDate[0]+"\n"+teamOne[0]+" vs "+teamTwo[0], 
            		stadiumName[0]+", "+stadiumCity[0]);
        displayOverlay.addOverlay(overlayitem);
        mapOverlays.add(displayOverlay);       
    }

	//Alternates between Satellite and Traffic View
    public boolean onKeyDown(int keyCode, KeyEvent e)
    {
        if(keyCode == KeyEvent.KEYCODE_S)
        {
            mapView.setSatellite(!mapView.isSatellite());
            return true;
        }
        else if(keyCode == KeyEvent.KEYCODE_T)
        {
            mapView.setTraffic(!mapView.isTraffic());
            mapController.animateTo(geoPoint); 
        }
        else if(keyCode == KeyEvent.KEYCODE_C)
        {
        	Intent calendarIntent = new Intent(Intent.ACTION_EDIT);
        	calendarIntent.setType("vnd.android.cursor.item/event");
        	calendarIntent.putExtra("allDay", false);
        	calendarIntent.putExtra("title", displayString);
            startActivity(calendarIntent);
        }
        
        return(super.onKeyDown(keyCode, e));
    }
    
    public void onBackPressed() 
    {
    	// TODO Auto-generated method stub
    	super.onBackPressed();
    	finish();
    }
    
	@Override
	protected boolean isRouteDisplayed() 
	{
		// TODO Auto-generated method stub
		return false;
	}
}
