package edu.columbia.sportsleagueandroid.appengine.beans;

public class Bowler 
{
	private String bowlerName;
	private int bowlerWickets;
	private double oversBowled;
	private int runsConceded;
	private double rate;
	
	public String getBowlerName() 
	{
		return bowlerName;
	}
	
	public void setBowlerName(String bowlerName) 
	{
		this.bowlerName = bowlerName;
	}
	
	public int getBowlerWickets() 
	{
		return bowlerWickets;
	}
	
	public void setBowlerWickets(int bowlerWickets)
	{
		this.bowlerWickets = bowlerWickets;
	}
	
	public double getOversBowled() 
	{
		return oversBowled;
	}
	
	public void setOversBowled(double oversBowled) 
	{
		this.oversBowled = oversBowled;
	}
	
	public int getRunsConceded()
	{
		return runsConceded;
	}
	
	public void setRunsConceded(int runsConceded) 
	{
		this.runsConceded = runsConceded;
	}
	
	public double getRate()
	{
		return rate;
	}
	
	public void setRate(double rate)
	{
		this.rate = rate;
	}	
}
