package edu.columbia.sportsleagueandroid.appengine.beans;

public class Batsman 
{
	private String batsmanName;
	private int runsScored;
	private int ballsPlayed;
	private double strikeRate;
	private int singles;
	private int fours;
	private int sixes;
	
	public String getBatsmanName() 
	{
		return batsmanName;
	}
	
	public void setBatsmanName(String batsmanName)
	{
		this.batsmanName = batsmanName;
	}
	
	public int getRunsScored()
	{
		return runsScored;
	}
	
	public void setRunsScored(int runsScored)
	{
		this.runsScored = runsScored;
	}
	
	public int getBallsPlayed() 
	{
		return ballsPlayed;
	}
	
	public void setBallsPlayed(int ballsPlayed) 
	{
		this.ballsPlayed = ballsPlayed;
	}
	public double getStrikeRate() 
	{
		return strikeRate;
	}
	
	public void setStrikeRate(double strikeRate)
	{
		this.strikeRate = strikeRate;
	}

	public int getSingles() 
	{
		return singles;
	}

	public void setSingles(int singles)
	{
		this.singles = singles;
	}

	public int getFours() 
	{
		return fours;
	}

	public void setFours(int fours) 
	{
		this.fours = fours;
	}

	public int getSixes() 
	{
		return sixes;
	}

	public void setSixes(int sixes) 
	{
		this.sixes = sixes;
	}
}
