package edu.columbia.sportsleagueandroid.server;

import java.util.HashMap;

import com.google.appengine.api.memcache.MemcacheService;
import com.google.appengine.api.memcache.MemcacheServiceFactory;

public class MemcacheController {

	private static MemcacheController instance = null;
	public HashMap<String, Object> syncCache;

	private MemcacheController() {

	}

	public static MemcacheController getInstance() {

		if (instance == null) {
			instance = new MemcacheController();
			instance.syncCache = new HashMap<String, Object>();
			return instance;
		}
		return instance;
	}

}
