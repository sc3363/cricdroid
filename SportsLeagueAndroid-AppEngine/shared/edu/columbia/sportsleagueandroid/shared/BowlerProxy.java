package edu.columbia.sportsleagueandroid.shared;

import com.google.web.bindery.requestfactory.shared.ProxyForName;
import com.google.web.bindery.requestfactory.shared.ValueProxy;

@ProxyForName(value = "edu.columbia.sportsleagueandroid.server.Bowler", locator = "edu.columbia.sportsleagueandroid.server.BowlerLocator")
public interface BowlerProxy extends ValueProxy {

	String getBowlerName();

	void setBowlerName(String bowlerName);

	int getBowlerWickets();

	void setBowlerWickets(int bowlerWickets);

	double getOversBowled();

	void setOversBowled(double oversBowled);

	int getRunsConceded();

	void setRunsConceded(int runsConceded);

	double getRate();

	void setRate(double rate);

}
