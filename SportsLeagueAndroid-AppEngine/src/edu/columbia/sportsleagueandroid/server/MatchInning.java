package edu.columbia.sportsleagueandroid.server;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class MatchInning
{
	@Id
	private int matchNumber;

	private int inningNumber;
	private String battingTeam;
	private String fieldingTeam;
	private int score;
	private int wickets;
	private double overs;
	
	public MatchInning()
	{}
	
	public int getMatchNumber() 
	{
		return matchNumber;
	}
	
	public void setMatchNumber(int matchNumber)
	{
		this.matchNumber = matchNumber;
	}
	
	public int getInningNumber() 
	{
		return inningNumber;
	}
	
	public void setInningNumber(int inningNumber) 
	{
		this.inningNumber = inningNumber;
	}
	
	public String getBattingTeam() 
	{
		return battingTeam;
	}
	
	public void setBattingTeam(String battingTeam)
	{
		this.battingTeam = battingTeam;
	}
	
	public String getFieldingTeam() 
	{
		return fieldingTeam;
	}
	
	public void setFieldingTeam(String fieldingTeam) 
	{
		this.fieldingTeam = fieldingTeam;
	}

	public int getScore() 
	{
		return score;
	}

	public void setScore(int score) 
	{
		this.score = score;
	}

	public int getWickets() 
	{
		return wickets;
	}

	public void setWickets(int wickets) 
	{
		this.wickets = wickets;
	}

	public double getOvers() 
	{
		return overs;
	}

	public void setOvers(double overs) 
	{
		this.overs = overs;
	}
}
