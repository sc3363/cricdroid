package edu.columbia.sportsleagueandroid.server;

import com.google.web.bindery.requestfactory.shared.Locator;


public class MatchInningLocator extends Locator<MatchInning, Void> {

	@Override
	public MatchInning create(Class<? extends MatchInning> clazz) {
		return new MatchInning();
	}

	@Override
	public MatchInning find(Class<? extends MatchInning> clazz, Void id) {
		return create(clazz);
	}

	@Override
	public Class<MatchInning> getDomainType() {
		return MatchInning.class;
	}

	@Override
	public Void getId(MatchInning domainObject) {
		return null;
	}

	@Override
	public Class<Void> getIdType() {
		return Void.class;
	}

	@Override
	public Object getVersion(MatchInning domainObject) {
		return null;
	}

}
