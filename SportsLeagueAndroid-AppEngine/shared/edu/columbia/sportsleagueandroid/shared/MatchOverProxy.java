package edu.columbia.sportsleagueandroid.shared;

import com.google.web.bindery.requestfactory.shared.ProxyForName;
import com.google.web.bindery.requestfactory.shared.ValueProxy;

@ProxyForName(value = "edu.columbia.sportsleagueandroid.server.MatchOver", locator = "edu.columbia.sportsleagueandroid.server.MatchOverLocator")
public interface MatchOverProxy extends ValueProxy {

	int getMatchNumber();

	void setMatchNumber(int matchNumber);

	int getInningNumber();

	void setInningNumber(int inningNumber);

	int getOverNumber();

	void setOverNumber(int overNumber);

	int getBallNumber();

	void setBallNumber(int ballNumber);

	String getBallTime();

	void setBallTime(String ballTime);

	String getAction();

	void setAction(String action);

	String getBatsman();

	void setBatsman(String batsman);

	String getNonStriker();

	void setNonStriker(String nonStriker);

	String getBowler();

	void setBowler(String bowler);

	String getComment();

	void setComment(String comment);

	int getRunsScored();

	void setRunsScored(int runsScored);

}
