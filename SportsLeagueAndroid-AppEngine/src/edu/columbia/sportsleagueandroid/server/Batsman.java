package edu.columbia.sportsleagueandroid.server;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Batsman 
{
	@Id
	private String batsmanName;
	private int runsScored;
	private int ballsPlayed;
	private double strikeRate;
	private String status;
	private int fours;
	private int sixes;
	
	public Batsman()
	{}
	
	public String getBatsmanName() 
	{
		return batsmanName;
	}
	
	public void setBatsmanName(String batsmanName)
	{
		this.batsmanName = batsmanName;
	}
	
	public int getRunsScored()
	{
		return runsScored;
	}
	
	public void setRunsScored(int runsScored)
	{
		this.runsScored = runsScored;
	}
	
	public int getBallsPlayed() 
	{
		return ballsPlayed;
	}
	
	public void setBallsPlayed(int ballsPlayed) 
	{
		this.ballsPlayed = ballsPlayed;
	}
	public double getStrikeRate() 
	{
		return strikeRate;
	}
	
	public void setStrikeRate(double strikeRate)
	{
		this.strikeRate = strikeRate;
	}

	public String getStatus() 
	{
		return status;
	}

	public void setStatus(String status)
	{
		this.status = status;
	}

	public int getFours() 
	{
		return fours;
	}

	public void setFours(int fours) 
	{
		this.fours = fours;
	}

	public int getSixes() 
	{
		return sixes;
	}

	public void setSixes(int sixes) 
	{
		this.sixes = sixes;
	}
}
