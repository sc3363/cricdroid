// Automatically Generated -- DO NOT EDIT
// edu.columbia.sportsleagueandroid.client.MyRequestFactory
package edu.columbia.sportsleagueandroid.client;
import java.util.Arrays;
import com.google.web.bindery.requestfactory.vm.impl.OperationData;
import com.google.web.bindery.requestfactory.vm.impl.OperationKey;
public final class MyRequestFactoryDeobfuscatorBuilder extends com.google.web.bindery.requestfactory.vm.impl.Deobfuscator.Builder {
{
withRawTypeToken("3HVlCDi2C_VK$X14oFgUbm9TVJI=", "edu.columbia.sportsleagueandroid.shared.BatsmanProxy");
withRawTypeToken("awKnik$bFbSuAaQZ32Jzdu0dPPg=", "edu.columbia.sportsleagueandroid.shared.BowlerProxy");
withRawTypeToken("ioyhtdeYChaI64StldbnwabLq60=", "edu.columbia.sportsleagueandroid.shared.MasterScorecardProxy");
withRawTypeToken("eQ9Y76UU8OqjsAEdl6Uc0lPXcok=", "edu.columbia.sportsleagueandroid.shared.MatchInningProxy");
withRawTypeToken("IiEuKI$_ALyOsb$xAwgZfhpI4Us=", "edu.columbia.sportsleagueandroid.shared.MatchOverProxy");
withRawTypeToken("DBR3$QrYNa7tO2JwG50UxP15qi0=", "edu.columbia.sportsleagueandroid.shared.MatchScheduleProxy");
withRawTypeToken("jX4WekfZZynciImPq89vbLh5iCE=", "edu.columbia.sportsleagueandroid.shared.MessageProxy");
withRawTypeToken("TcoSUTcwxzu4bsAFz9MMfQ56KW8=", "edu.columbia.sportsleagueandroid.shared.NewsProxy");
withRawTypeToken("zzckLhT2K3Rw6bI4cz2azIdlMIk=", "edu.columbia.sportsleagueandroid.shared.PlayerProxy");
withRawTypeToken("7BtNwpkS6O4VevRcu1K4V8qz7z8=", "edu.columbia.sportsleagueandroid.shared.RegistrationInfoProxy");
withRawTypeToken("HXOYp5qDQXzR0AE6LnlAyX3izxg=", "edu.columbia.sportsleagueandroid.shared.StadiumProxy");
withRawTypeToken("0qhpRo9E8PEFNqaYl6pbM7fus0g=", "edu.columbia.sportsleagueandroid.shared.TeamProxy");
withRawTypeToken("8KVVbwaaAtl6KgQNlOTsLCp9TIU=", "com.google.web.bindery.requestfactory.shared.ValueProxy");
withRawTypeToken("FXHD5YU0TiUl3uBaepdkYaowx9k=", "com.google.web.bindery.requestfactory.shared.BaseProxy");
}}
