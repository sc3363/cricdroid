package edu.columbia.sportsleagueandroid.server;

import com.google.web.bindery.requestfactory.shared.Locator;


public class StadiumLocator extends Locator<Stadium, Void> {

	@Override
	public Stadium create(Class<? extends Stadium> clazz) {
		return new Stadium();
	}

	@Override
	public Stadium find(Class<? extends Stadium> clazz, Void id) {
		return create(clazz);
	}

	@Override
	public Class<Stadium> getDomainType() {
		return Stadium.class;
	}

	@Override
	public Void getId(Stadium domainObject) {
		return null;
	}

	@Override
	public Class<Void> getIdType() {
		return Void.class;
	}

	@Override
	public Object getVersion(Stadium domainObject) {
		return null;
	}

}
