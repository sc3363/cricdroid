/*******************************************************************************
 * Copyright 2011 Google Inc. All Rights Reserved.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package edu.columbia.sportsleagueandroid.client;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.DivElement;
import com.google.gwt.dom.client.InputElement;
import com.google.gwt.dom.client.TextAreaElement;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.Widget;
import com.google.web.bindery.event.shared.EventBus;
import com.google.web.bindery.event.shared.SimpleEventBus;
import com.google.web.bindery.requestfactory.shared.Receiver;
import com.google.web.bindery.requestfactory.shared.Request;
import com.google.web.bindery.requestfactory.shared.ServerFailure;

import edu.columbia.sportsleagueandroid.client.MyRequestFactory.HelloWorldRequest;
import edu.columbia.sportsleagueandroid.client.MyRequestFactory.MessageRequest;
import edu.columbia.sportsleagueandroid.shared.MatchOverProxy;
import edu.columbia.sportsleagueandroid.shared.MessageProxy;
import edu.columbia.sportsleagueandroid.shared.SportsLeagueAndroidRequest;
import edu.columbia.sportsleagueandroid.shared.StadiumProxy;

public class SportsLeagueAndroidWidget extends Composite 
{

  public boolean tempResult = false;
  public String finalResponse;
  
  private static final int STATUS_DELAY = 4000;
  private static final String STATUS_ERROR = "status error";
  private static final String STATUS_NONE = "status none";
  private static final String STATUS_SUCCESS = "status success";

  interface SportsLeagueAndroidUiBinder extends UiBinder<Widget, SportsLeagueAndroidWidget> 
  {
  }

  private static SportsLeagueAndroidUiBinder uiBinder = GWT.create(SportsLeagueAndroidUiBinder.class);

  @UiField
  TextAreaElement messageArea;
  
  
  @UiField
  TextAreaElement recipientArea;

  @UiField
  DivElement status;

  @UiField
  Button sayHelloButton;

  @UiField
  Button update;


  @UiField
  Button stadium;
  
  @UiField
  Button match;
  
  @UiField
  Button score;
  /**
   * Timer to clear the UI.
   */
  Timer timer = new Timer() 
  {
    @Override
    public void run() 
    {
      status.setInnerText("");
      status.setClassName(STATUS_NONE);
      recipientArea.setValue("");
      messageArea.setValue("");
    }
  };

  private void setStatus(String message, boolean error) 
  {
    status.setInnerText(message);
    if (error) 
    {
      status.setClassName(STATUS_ERROR);
    } 
    else 
    {
      if (message.length() == 0) 
      {
        status.setClassName(STATUS_NONE);
      } 
      else 
      {
        status.setClassName(STATUS_SUCCESS);
      }
    }

    timer.schedule(STATUS_DELAY);
  }

  public SportsLeagueAndroidWidget() 
  {
    initWidget(uiBinder.createAndBindUi(this));
    sayHelloButton.getElement().setClassName("send centerbtn");
   
    final EventBus eventBus = new SimpleEventBus();
    final MyRequestFactory requestFactory = GWT.create(MyRequestFactory.class);
    requestFactory.initialize(eventBus);
    
    
    stadium.addClickHandler(new ClickHandler() 
    {
    public void onClick(ClickEvent event) 
    {
        //String recipient = recipientArea.getValue();
        //String receivers[] = recipient.split(",");
        String message = messageArea.getValue();
       // setStatus("Connecting...", false);
        update.setEnabled(false);
        match.setEnabled(false);
        score.setEnabled(false);
        sendNewTaskToServer(message);
    }
    private void sendNewTaskToServer(String message) 
    {
        SportsLeagueAndroidRequest request = requestFactory.sportsLeagueAndroidRequest();
        StadiumProxy task = request.create(StadiumProxy.class);
       // int len = Math.min(message.length(), 50);
        String stad [] = message.split(",");
        task.setStadiumCapacity(Long.parseLong(stad[3]));
        task.setStadiumCity(stad[1]);
        task.setStadiumCountry(stad[2]);
        task.setStadiumName(stad[0]);
        //setStatus("Connecting", false);
        setStatus(task.toString(), false);
        request.updateStadium(task).fire();
        //tasksList.add(task);
        
      }
    
    });
      
    
    update.addClickHandler(new ClickHandler() 
    {
    public void onClick(ClickEvent event) 
    {
        String recipient = recipientArea.getValue();
        String receivers[] = recipient.split(",");
        String message = messageArea.getValue();
        String splitMessage[] = message.split(",");
        setStatus("Connecting...", false);
        stadium.setEnabled(false);
        score.setEnabled(false);
        match.setEnabled(false);
        /* Code added to add match over to database, update player score, player records and match scoreboard*/
        SportsLeagueAndroidRequest spRequest = requestFactory.sportsLeagueAndroidRequest();
        MatchOverProxy task = spRequest.create(MatchOverProxy.class);
        
        task.setMatchNumber(Integer.parseInt(splitMessage[0]));
        task.setInningNumber(Integer.parseInt(splitMessage[1]));
        //task.setBallNumber(Integer.parseInt(splitMessage[2]));
        task.setBallTime("sysdate()");
        task.setAction(splitMessage[2]);
        task.setBatsman(splitMessage[3]);
        task.setBowler(splitMessage[4]);
        task.setComment(splitMessage[5]);
        task.setRunsScored(Integer.parseInt(splitMessage[6]));
        
        setStatus(task.toString(), false);
        spRequest.updateMatchOver(task).fire();
        /* -------------------------------------- Added code ends here --------------------------------------- */
        
      }
    });
    
    match.addClickHandler(new ClickHandler() 
    {
    public void onClick(ClickEvent event) 
    {
        String recipient = recipientArea.getValue();
        String receivers[] = recipient.split(",");
        String message = messageArea.getValue();
        String splitMessage[] = message.split(",");
        setStatus("Connecting...", false);
        stadium.setEnabled(false);
        score.setEnabled(false);
        match.setEnabled(false);
        /* Code added to add match over to database, update player score, player records and match scoreboard*/
        SportsLeagueAndroidRequest spRequest = requestFactory.sportsLeagueAndroidRequest();
        MatchOverProxy task = spRequest.create(MatchOverProxy.class);
        
        task.setMatchNumber(Integer.parseInt(splitMessage[0]));
        task.setInningNumber(Integer.parseInt(splitMessage[1]));
        task.setBallNumber(Integer.parseInt(splitMessage[2]));
        task.setBallTime("sysdate()");
        task.setAction(splitMessage[3]);
        task.setBatsman(splitMessage[4]);
        task.setBowler(splitMessage[5]);
        task.setComment(splitMessage[6]);
        task.setRunsScored(Integer.parseInt(splitMessage[7]));
        
        setStatus(task.toString(), false);
        spRequest.updateMatchOver(task).fire();
        /* -------------------------------------- Added code ends here --------------------------------------- */
        
      }
    });
    
    sayHelloButton.addClickHandler(new ClickHandler() 
    {
      public void onClick(ClickEvent event) 
      {
        sayHelloButton.setEnabled(false);
        HelloWorldRequest helloWorldRequest = requestFactory.helloWorldRequest();
        helloWorldRequest.getMessage().fire(new Receiver<String>() 
        {
          @Override
          public void onFailure(ServerFailure error)
          {
            sayHelloButton.setEnabled(true);
            setStatus(error.getMessage(), true);
          }

          @Override
          public void onSuccess(String response) 
          {
            sayHelloButton.setEnabled(true);
            setStatus(response, response.startsWith("Failure:"));
          }
        });
      }
    });
  }
    
  
}

