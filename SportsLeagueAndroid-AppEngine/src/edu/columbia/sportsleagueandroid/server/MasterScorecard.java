package edu.columbia.sportsleagueandroid.server;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class MasterScorecard 
{
	@Id
	private int matchNumber;
	private String teamOne;
	private String teamTwo;
	private String stadiumName;
	private String stadiumCity;
	private List<Batsman> batsman;
	private List<Bowler> bowler;
	private int score;
	private int wickets;
	private double overs;
	
	public MasterScorecard()
	{}
	
	public int getMatchNumber() 
	{
		return matchNumber;
	}
	
	public void setMatchNumber(int matchNumber) 
	{
		this.matchNumber = matchNumber;
	}
	
	public String getTeamOne() 
	{
		return teamOne;
	}
	
	public void setTeamOne(String teamOne) 
	{
		this.teamOne = teamOne;
	}
	
	public String getTeamTwo() 
	{
		return teamTwo;
	}
	
	public void setTeamTwo(String teamTwo) 
	{
		this.teamTwo = teamTwo;
	}
	
	public String getStadiumName() 
	{
		return stadiumName;
	}
	
	public void setStadiumName(String stadiumName) 
	{
		this.stadiumName = stadiumName;
	}
	
	public String getStadiumCity() 
	{
		return stadiumCity;
	}
	
	public void setStadiumCity(String stadiumCity) 
	{
		this.stadiumCity = stadiumCity;
	}
	
	public int getScore() 
	{
		return score;
	}
	
	public void setScore(int score) 
	{
		this.score = score;
	}
	
	public int getWickets() 
	{
		return wickets;
	}
	
	public void setWickets(int wickets) 
	{
		this.wickets = wickets;
	}
	
	public double getOvers()
	{
		return overs;
	}
	
	public void setOvers(double overs) 
	{
		this.overs = overs;
	}

	public List<Batsman> getBatsman() 
	{
		return batsman;
	}

	public void setBatsman(List<Batsman> batsman)
	{
		this.batsman = batsman;
	}

	public List<Bowler> getBowler() 
	{
		return bowler;
	}

	public void setBowler(List<Bowler> bowler)
	{
		this.bowler = bowler;
	}

	
}
