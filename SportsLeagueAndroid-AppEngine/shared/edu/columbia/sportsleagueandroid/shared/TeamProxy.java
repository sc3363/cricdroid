package edu.columbia.sportsleagueandroid.shared;

import com.google.web.bindery.requestfactory.shared.ProxyForName;
import com.google.web.bindery.requestfactory.shared.ValueProxy;

@ProxyForName(value = "edu.columbia.sportsleagueandroid.server.Team", locator = "edu.columbia.sportsleagueandroid.server.TeamLocator")
public interface TeamProxy extends ValueProxy {

	String getTeamName();

	void setTeamName(String teamName);

	int getTeamRanking();

	void setTeamRanking(int teamRanking);

	int getMatchCount();

	void setMatchCount(int matchCount);

	int getWins();

	void setWins(int wins);

	int getLosses();

	void setLosses(int losses);

	int getNoResult();

	void setNoResult(int noResult);

}
