/*******************************************************************************
 * Copyright 2011 Google Inc. All Rights Reserved.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package edu.columbia.sportsleagueandroid.client;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Grid;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.TextBox;

/**
 * Entry point classes define <code>onModuleLoad()</code>.
 */
public class SportsLeagueAndroid implements EntryPoint {

  /**
   * This is the entry point method.
   */
	Label nameLabel = new Label("Name:");
	   TextBox nameBox = new TextBox();
	   Label addrLabel = new Label("Address:");
	   TextBox addrBox = new TextBox();
	   Label phoneLabel = new Label("Phone number:");
	   TextBox phoneBox = new TextBox();
	   Button button = new Button("Submit");
	   ListBox lb = new ListBox();
	   Label runs = new Label("Enter runs:");
	   TextBox runBox = new TextBox();

	    // Make enough room for all five items (setting this value to 1 turns it
	    // into a drop-down list).
	   Grid grid = new Grid(20, 20);
  public void onModuleLoad()
  {
    SportsLeagueAndroidWidget widget = new SportsLeagueAndroidWidget();
  
      RootPanel.get().add(widget);
  }
}
