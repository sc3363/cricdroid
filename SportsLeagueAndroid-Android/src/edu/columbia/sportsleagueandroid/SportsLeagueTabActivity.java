package edu.columbia.sportsleagueandroid;

import android.app.TabActivity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.TabHost;
import android.widget.TabHost.TabSpec;
 
public class SportsLeagueTabActivity extends TabActivity 
{
    /** Called when the activity is first created. */
	
	@Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.tab_home);
 
        TabHost tabHost = new TabHost(this);
        tabHost = getTabHost();
        
        TabSpec homespec = tabHost.newTabSpec("NewsActivityGroup");
        // setting Title and Icon for the Tab
        homespec.setIndicator("Latest Cricketing News", getResources().getDrawable(R.drawable.icon_home_tab));
        Intent homeIntent = new Intent(this, NewsActivityGroup.class);
        homespec.setContent(homeIntent);
        
        TabSpec livespec = tabHost.newTabSpec("MatchScoreActivityGroup");
        // setting Title and Icon for the Tab
        livespec.setIndicator("", getResources().getDrawable(R.drawable.icon_live_tab));
        Intent liveIntent = new Intent(this, MatchScoreActivityGroup.class);
        livespec.setContent(liveIntent);
 
        TabSpec fixturespec = tabHost.newTabSpec("FixturesActivityGroup");
        fixturespec.setIndicator("Fixtures", getResources().getDrawable(R.drawable.icon_fixtures_tab));
        Intent fixturesIntent = new Intent(this, FixturesActivityGroup.class);
        fixturespec.setContent(fixturesIntent);
 
        TabSpec resultsspec = tabHost.newTabSpec("ResultsActivityGroup");
        resultsspec.setIndicator("Results", getResources().getDrawable(R.drawable.icon_results_tab));
        Intent resultsIntent = new Intent(this, ResultsActivityGroup.class);
        resultsspec.setContent(resultsIntent);
 
        // Adding all TabSpec to TabHost
        tabHost.addTab(homespec);
        tabHost.addTab(livespec); 
        tabHost.addTab(fixturespec); 
        tabHost.addTab(resultsspec); 
    }
}