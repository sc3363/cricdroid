package edu.columbia.sportsleagueandroid.shared;

import com.google.web.bindery.requestfactory.shared.ProxyForName;
import com.google.web.bindery.requestfactory.shared.ValueProxy;

@ProxyForName(value = "edu.columbia.sportsleagueandroid.server.Stadium", locator = "edu.columbia.sportsleagueandroid.server.StadiumLocator")
public interface StadiumProxy extends ValueProxy {

	String getStadiumName();

	void setStadiumName(String stadiumName);

	String getStadiumCity();

	void setStadiumCity(String stadiumCity);

	String getStadiumCountry();

	void setStadiumCountry(String stadiumCountry);

	long getStadiumCapacity();

	void setStadiumCapacity(long stadiumCapacity);

	double getLatitude();

	void setLatitude(double latitude);

	double getLongitude();

	void setLongitude(double longitude);

}
