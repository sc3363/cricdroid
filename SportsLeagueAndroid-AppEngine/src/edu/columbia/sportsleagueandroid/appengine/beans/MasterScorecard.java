package edu.columbia.sportsleagueandroid.appengine.beans;

import edu.columbia.sportsleagueandroid.server.Batsman;
import edu.columbia.sportsleagueandroid.server.Bowler;

public class MasterScorecard 
{
	private int matchNumber;
	private int inningNumber;
	private String teamOne;
	private String teamTwo;
	private String stadiumName;
	private String stadiumCity;
	private Batsman batsman1;
	private Batsman batsman2;
	private Bowler bowler1;
	private Bowler bowler2;
	private int score;
	private int wickets;
	private double overs;
	
	public int getMatchNumber() 
	{
		return matchNumber;
	}
	
	public void setMatchNumber(int matchNumber) 
	{
		this.matchNumber = matchNumber;
	}
	
	public int getInningNumber()
	{
		return inningNumber;
	}
	
	public void setInningNumber(int inningNumber) 
	{
		this.inningNumber = inningNumber;
	}
	
	public String getTeamOne() 
	{
		return teamOne;
	}
	
	public void setTeamOne(String teamOne) 
	{
		this.teamOne = teamOne;
	}
	
	public String getTeamTwo() 
	{
		return teamTwo;
	}
	
	public void setTeamTwo(String teamTwo) 
	{
		this.teamTwo = teamTwo;
	}
	
	public String getStadiumName() 
	{
		return stadiumName;
	}
	
	public void setStadiumName(String stadiumName) 
	{
		this.stadiumName = stadiumName;
	}
	
	public String getStadiumCity() 
	{
		return stadiumCity;
	}
	
	public void setStadiumCity(String stadiumCity) 
	{
		this.stadiumCity = stadiumCity;
	}
	
	public Batsman getBatsman1() 
	{
		return batsman1;
	}
	
	public void setBatsman(Batsman batsman1) 
	{
		this.batsman1 = batsman1;
	}
	
	public Batsman getBatsman2() 
	{
		return batsman2;
	}
	
	public void setNonStriker(Batsman batsman2) 
	{
		this.batsman2 = batsman2;
	}
	
	public Bowler getBowler1() 
	{
		return bowler1;
	}
	
	public void setBowler1(Bowler bowler1)
	{
		this.bowler1 = bowler1;
	}
	
	public Bowler getBowler2() 
	{
		return bowler2;
	}
	
	public void setBowler2(Bowler bowler2)
	{
		this.bowler2 = bowler2;
	}
	
	public int getScore() 
	{
		return score;
	}
	
	public void setScore(int score) 
	{
		this.score = score;
	}
	
	public int getWickets() 
	{
		return wickets;
	}
	
	public void setWickets(int wickets) 
	{
		this.wickets = wickets;
	}
	
	public double getOvers()
	{
		return overs;
	}
	
	public void setOvers(double overs) 
	{
		this.overs = overs;
	}
	
	
	
}