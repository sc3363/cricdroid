package edu.columbia.sportsleagueandroid.server;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.SQLNonTransientException;
import java.sql.SQLRecoverableException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.servlet.ServletContext;

import com.google.appengine.api.memcache.MemcacheService;
import com.google.gwt.dev.js.rhino.ObjToIntMap.Iterator;
import com.google.web.bindery.requestfactory.server.RequestFactoryServlet;

import edu.columbia.sportsleagueandroid.annotation.ServiceMethod;

public class SportsLeagueAndroidService {

	@ServiceMethod
	public Batsman createBatsman() {
		// TODO Auto-generated method stub
		return null;
	}

	@ServiceMethod
	public Batsman readBatsman(Long id) {
		// TODO Auto-generated method stub
		return null;
	}

	@ServiceMethod
	public Batsman updateBatsman(String email_id,int matchno, String preferences ) {

		saveUserPreferences(matchno, email_id, preferences+",O");
		return null;
	}

	@ServiceMethod
	public void deleteBatsman(Batsman batsman) {
		// TODO Auto-generated method stub

	}

	@ServiceMethod
	public List<String> queryBatsmans(int matchno, String username) {

		List<String> pref = new ArrayList<String>();
		String list = fetchUserPreferences(matchno, username);
		if(list != null)
		{
		String [] y = list.split(",");
		
		for(String x : y)
			pref.add(x);
		}
		return pref;
	}

	@ServiceMethod
	public MatchOver createMatchOver() {
		// TODO Auto-generated method stub
		return null;
	}

	@ServiceMethod
	public MatchOver readMatchOver() {
		// TODO Auto-generated method stub
		return null;
	}

	@ServiceMethod
	public MatchOver updateMatchOver(MatchOver matchover)
	{
		ConnectionManager connectionManager = ConnectionManager.getInstance();
		Connection dbConnection  = connectionManager.getConnection();

		MatchInning matchInning = new MatchInning();

		try
		{
			ScoreboardActions actions = new ScoreboardActions();

			try 
			{
				matchInning = actions.UpdateOver(dbConnection, matchover);
				Set <String> users = null;
				matchInning.getMatchNumber();
				String over = matchInning.getOvers()+"";
				int pos=0;
				String displayString = "";
				if((pos=over.indexOf("."))!=-1)
				{
					String o = over.substring(0,pos+2);
					matchInning.setOvers(Double.parseDouble(o));
					if(o.equals("0"))
					{
						users = fetchUsersForAction(matchover.getMatchNumber(), "O");
						displayString = matchInning.getBattingTeam()+" "+matchInning.getScore()+"/"+matchInning.getWickets()+" | "+matchInning.getOvers()+" Overs \n"+
								"Run Rate: "+matchInning.getScore()/matchInning.getOvers()+" per over";
					}

					else if(matchover.getAction().equalsIgnoreCase("W"))
					{
						users = fetchUsersForAction(matchover.getMatchNumber(), "W");
						displayString = matchInning.getBattingTeam()+" "+matchInning.getScore()+"/"+matchInning.getWickets()+" \n"+
								matchover.getAction()+" "+matchover.getBatsman()+" b. "+matchover.getBowler()+"\n"+matchover.getComment();
					}
					else if(matchover.getRunsScored()==4)
					{
						users = fetchUsersForAction(matchover.getMatchNumber(), "4");
						displayString = matchInning.getBattingTeam()+" "+matchInning.getScore()+"/"+matchInning.getWickets()+" | "+matchInning.getOvers()+" Overs \n"+
								matchover.getRunsScored()+": "+matchover.getBatsman()+" off "+matchover.getBowler()+"\n"+matchover.getComment();
					}
					else if(matchover.getRunsScored()==6)
					{
						users = fetchUsersForAction(matchover.getMatchNumber(), "6");
						displayString = matchInning.getBattingTeam()+" "+matchInning.getScore()+"/"+matchInning.getWickets()+" | "+matchInning.getOvers()+" Overs \n"+
								matchover.getRunsScored()+": "+matchover.getBatsman()+" off "+matchover.getBowler()+"\n"+matchover.getComment();
					}
				}

				if(users != null)
				{
					ServletContext context = RequestFactoryServlet.getThreadLocalRequest().getSession().getServletContext();
					for(String u : users)
					{
						SendMessage.sendMessage(context, u, displayString);
					}
				}
			}
			catch(SQLRecoverableException sqlre)
			{
				dbConnection.close();
				dbConnection = ConnectionManager.getFreshConnection();
				matchInning = actions.UpdateOver(dbConnection, matchover);
			}
			catch(SQLNonTransientException sqlnte)
			{
				dbConnection.close();
				dbConnection = ConnectionManager.getFreshConnection();
				matchInning = actions.UpdateOver(dbConnection, matchover);
			}
			catch(SQLException sqle)
			{
				connectionManager = ConnectionManager.getInstance();
				dbConnection = ConnectionManager.getFreshConnection();
				matchInning = actions.UpdateOver(dbConnection, matchover);
				sqle.printStackTrace();
			}
		}
		catch(SQLException sqle)
		{
			dbConnection = ConnectionManager.getFreshConnection();
			sqle.printStackTrace();
		}
		catch(IOException ioe)
		{
			ioe.printStackTrace();
		}

		return null;
	}

	@ServiceMethod
	public void deleteMatchOver(MatchOver matchover) {
		// TODO Auto-generated method stub

	}

	@ServiceMethod
	public List<MatchOver> queryMatchOvers() {
		// TODO Auto-generated method stub
		return null;
	}

	@ServiceMethod
	public Bowler createBowler() {
		// TODO Auto-generated method stub
		return null;
	}

	@ServiceMethod
	public Bowler readBowler(Long id) {
		// TODO Auto-generated method stub
		return null;
	}

	@ServiceMethod
	public Bowler updateBowler(Bowler bowler) {
		// TODO Auto-generated method stub
		return null;
	}

	@ServiceMethod
	public void deleteBowler(Bowler bowler) {
		// TODO Auto-generated method stub

	}

	@ServiceMethod
	public List<Bowler> queryBowlers() {
		// TODO Auto-generated method stub
		return null;
	}

	@ServiceMethod
	public MatchInning createMatchInning() {
		// TODO Auto-generated method stub
		return null;
	}

	@ServiceMethod
	public MatchInning readMatchInning(Long id) {
		// TODO Auto-generated method stub
		return null;
	}

	@ServiceMethod
	public MatchInning updateMatchInning(MatchInning matchinning) {
		// TODO Auto-generated method stub
		return null;
	}

	@ServiceMethod
	public void deleteMatchInning(MatchInning matchinning) {
		// TODO Auto-generated method stub

	}

	@ServiceMethod
	public List<MatchInning> queryMatchInnings(int matchNumber) 
	{
		ConnectionManager connectionManager = ConnectionManager.getInstance();
		Connection dbConnection  = connectionManager.getConnection();

		List<MatchInning> inning = new ArrayList<MatchInning>();

		try
		{
			ScoreboardActions actions = new ScoreboardActions();

			try 
			{
				inning = actions.FetchScorePreview(dbConnection, matchNumber);
			}
			catch(SQLRecoverableException sqlre)
			{
				dbConnection.close();
				dbConnection = ConnectionManager.getFreshConnection();
				inning = actions.FetchScorePreview(dbConnection, matchNumber);
			}
			catch(SQLNonTransientException sqlnte)
			{
				dbConnection.close();
				dbConnection = ConnectionManager.getFreshConnection();
				inning = actions.FetchScorePreview(dbConnection, matchNumber);
			}
			catch(SQLException sqle)
			{
				connectionManager = ConnectionManager.getInstance();
				dbConnection = ConnectionManager.getFreshConnection();
				inning = actions.FetchScorePreview(dbConnection, matchNumber);
				sqle.printStackTrace();
			}
		}
		catch(SQLException sqle)
		{
			dbConnection = ConnectionManager.getFreshConnection();
			sqle.printStackTrace();
		}
		catch(IOException ioe)
		{
			ioe.printStackTrace();
		}

		return inning;
	}

	@ServiceMethod
	public Team createTeam() {
		// TODO Auto-generated method stub
		return null;
	}

	@ServiceMethod
	public Team readTeam(Long id) {
		// TODO Auto-generated method stub
		return null;
	}

	@ServiceMethod
	public Team updateTeam(Team team) {
		// TODO Auto-generated method stub
		return null;
	}

	@ServiceMethod
	public void deleteTeam(Team team) {
		// TODO Auto-generated method stub

	}

	@ServiceMethod
	public List<Team> queryTeams() {
		// TODO Auto-generated method stub
		return null;
	}

	@ServiceMethod
	public MasterScorecard createMasterScorecard() {
		// TODO Auto-generated method stub
		return null;
	}

	@ServiceMethod
	public MasterScorecard readMasterScorecard(Long id) {
		// TODO Auto-generated method stub
		return null;
	}

	@ServiceMethod
	public MasterScorecard updateMasterScorecard(MasterScorecard masterscorecard) {
		// TODO Auto-generated method stub
		return null;
	}

	@ServiceMethod
	public void deleteMasterScorecard(MasterScorecard masterscorecard) {
		// TODO Auto-generated method stub

	}

	@ServiceMethod
	public List<MasterScorecard> queryMasterScorecards(final int matchNumber, final int inningNumber, final String battingTeam, final String fieldingTeam, 
			final int teamScore, final int teamWickets, final double teamOvers, final String stadiumName,final String stadiumCity) 
			{
		ConnectionManager connectionManager = ConnectionManager.getInstance();
		Connection dbConnection  = connectionManager.getConnection();

		List<MasterScorecard> scorecard = new ArrayList<MasterScorecard>();

		try
		{
			ScoreboardActions action = new ScoreboardActions();

			try 
			{
				scorecard.add(action.FetchMasterScorecard(dbConnection, matchNumber, inningNumber, battingTeam, fieldingTeam, teamScore, teamWickets, teamOvers, 
						stadiumName, stadiumCity));
			}
			catch(SQLRecoverableException sqlre)
			{
				dbConnection.close();
				dbConnection = ConnectionManager.getFreshConnection();
				scorecard.add(action.FetchMasterScorecard(dbConnection, matchNumber, inningNumber, battingTeam, fieldingTeam, teamScore, teamWickets, teamOvers, 
						stadiumName, stadiumCity));			}
			catch(SQLNonTransientException sqlnte)
			{
				dbConnection.close();
				dbConnection = ConnectionManager.getFreshConnection();
				scorecard.add(action.FetchMasterScorecard(dbConnection, matchNumber, inningNumber, battingTeam, fieldingTeam, teamScore, teamWickets, teamOvers, 
						stadiumName, stadiumCity));
			}
			catch(SQLException sqle)
			{
				connectionManager = ConnectionManager.getInstance();
				dbConnection = ConnectionManager.getFreshConnection();
				scorecard.add(action.FetchMasterScorecard(dbConnection, matchNumber, inningNumber, battingTeam, fieldingTeam, teamScore, teamWickets, teamOvers, 
						stadiumName, stadiumCity));
				sqle.printStackTrace();
			}
		}
		catch(SQLException sqle)
		{
			dbConnection = ConnectionManager.getFreshConnection();
			sqle.printStackTrace();
		}
		catch(IOException ioe)
		{
			ioe.printStackTrace();
		}

		return scorecard;
			}


	@ServiceMethod
	public Stadium createStadium() {
		// TODO Auto-generated method stub
		return null;
	}

	@ServiceMethod
	public Stadium readStadium(Long id) {
		// TODO Auto-generated method stub
		return null;
	}

	@ServiceMethod
	public Stadium updateStadium(Stadium stadium) {
		// TODO Auto-generated method stub
		return null;
	}

	@ServiceMethod
	public void deleteStadium(Stadium stadium) {
		// TODO Auto-generated method stub

	}

	@ServiceMethod
	public List<Stadium> queryStadiums(String stadiumName) 
	{
		ConnectionManager connectionManager = ConnectionManager.getInstance();
		Connection dbConnection  = connectionManager.getConnection();

		List<Stadium> stadiumList = new ArrayList<Stadium>();

		ScoreboardActions s = new ScoreboardActions();
		try
		{
			try 
			{
				stadiumList = s.FetchStadiumDetails(dbConnection, stadiumName);
			}
			catch(SQLRecoverableException sqlre)
			{
				dbConnection.close();
				dbConnection = ConnectionManager.getFreshConnection();
				stadiumList = s.FetchStadiumDetails(dbConnection, stadiumName);
			}
			catch(SQLNonTransientException sqlnte)
			{
				dbConnection.close();
				dbConnection = ConnectionManager.getFreshConnection();
				stadiumList = s.FetchStadiumDetails(dbConnection, stadiumName);
			}
			catch(SQLException sqle)
			{
				connectionManager = ConnectionManager.getInstance();
				dbConnection = ConnectionManager.getFreshConnection();
				stadiumList = s.FetchStadiumDetails(dbConnection, stadiumName);
				sqle.printStackTrace();
			}
		}
		catch(IOException ioe)
		{
			ioe.printStackTrace();
		}
		catch(SQLException sqle)
		{
			dbConnection = ConnectionManager.getFreshConnection();
			sqle.printStackTrace();
		}

		return stadiumList;
	}

	@ServiceMethod
	public Player createPlayer() {
		// TODO Auto-generated method stub
		return null;
	}

	@ServiceMethod
	public Player readPlayer(Long id) {
		// TODO Auto-generated method stub
		return null;
	}

	@ServiceMethod
	public Player updatePlayer(Player player) {
		// TODO Auto-generated method stub
		return null;
	}

	@ServiceMethod
	public void deletePlayer(Player player) {
		// TODO Auto-generated method stub

	}

	@ServiceMethod
	public List<Player> queryPlayers() {
		// TODO Auto-generated method stub
		return null;
	}


	@ServiceMethod
	public News createNews() {
		// TODO Auto-generated method stub
		return null;
	}

	@ServiceMethod
	public News readNews(Long id) {
		// TODO Auto-generated method stub
		return null;
	}

	@ServiceMethod
	public News updateNews(News news) {
		// TODO Auto-generated method stub
		return null;
	}

	@ServiceMethod
	public void deleteNews(News news) {
		// TODO Auto-generated method stub

	}

	@ServiceMethod
	public List<News> queryNewss(String type) 
	{
		ConnectionManager connectionManager = ConnectionManager.getInstance();
		Connection dbConnection  = connectionManager.getConnection();

		List<News> newsFeed = new ArrayList<News>();

		try
		{
			try 
			{
				ScoreboardActions actions = new ScoreboardActions();
				if(type.equalsIgnoreCase("Headlines"))
				{
					try
					{
						newsFeed = actions.FetchNews(dbConnection);
					}
					catch(SQLRecoverableException sqlre)
					{
						dbConnection.close();
						dbConnection = ConnectionManager.getFreshConnection();
						newsFeed = actions.FetchNews(dbConnection);
					}
					catch(SQLNonTransientException sqlnte)
					{
						dbConnection.close();
						dbConnection = ConnectionManager.getFreshConnection();
						newsFeed = actions.FetchNews(dbConnection);
					}
					catch(SQLException sqle)
					{
						connectionManager = ConnectionManager.getInstance();
						dbConnection = ConnectionManager.getFreshConnection();
						newsFeed = actions.FetchNews(dbConnection);
						sqle.printStackTrace();
					}
				}
				else
				{
					try
					{
						newsFeed = actions.FetchDetailedStory(dbConnection, type);
					}
					catch(SQLRecoverableException sqlre)
					{
						dbConnection.close();
						dbConnection = ConnectionManager.getFreshConnection();
						newsFeed = actions.FetchDetailedStory(dbConnection, type);
					}
					catch(SQLNonTransientException sqlnte)
					{
						dbConnection.close();
						dbConnection = ConnectionManager.getFreshConnection();
						newsFeed = actions.FetchDetailedStory(dbConnection, type);
					}
					catch(SQLException sqle)
					{
						dbConnection = ConnectionManager.getFreshConnection();
						newsFeed = actions.FetchDetailedStory(dbConnection, type);
						sqle.printStackTrace();
					}
				}
			}
			catch(IOException ioe)
			{
				dbConnection.close();
				ioe.printStackTrace();
			}
		}
		catch(SQLException sqle)
		{
			sqle.printStackTrace();
			dbConnection = ConnectionManager.getFreshConnection();
		}

		return newsFeed;
	}


	@ServiceMethod
	public MatchSchedule createMatchSchedule() {
		// TODO Auto-generated method stub
		return null;
	}

	@ServiceMethod
	public MatchSchedule readMatchSchedule(Long id) {
		// TODO Auto-generated method stub
		return null;
	}

	@ServiceMethod
	public MatchSchedule updateMatchSchedule(MatchSchedule matchschedule) {
		// TODO Auto-generated method stub
		return null;
	}

	@ServiceMethod
	public void deleteMatchSchedule(MatchSchedule matchschedule) {
		// TODO Auto-generated method stub

	}

	@ServiceMethod
	public List<MatchSchedule> queryMatchSchedules(String type) 
	{
		ConnectionManager connectionManager = ConnectionManager.getInstance();
		Connection dbConnection  = connectionManager.getConnection();

		List<MatchSchedule> mSchedule = new ArrayList<MatchSchedule>();

		try
		{
			ScoreboardActions s = new ScoreboardActions();

			try 
			{
				if(type.equalsIgnoreCase("Future"))
				{
					mSchedule = s.FetchFutureMatchSchedule(dbConnection);
				}
				else if(type.equalsIgnoreCase("Result"))
				{
					mSchedule = s.FetchResults(dbConnection);
				}
				else
				{
					mSchedule = s.FetchCurrentDaySchedule(dbConnection);
				}
			}
			catch(SQLRecoverableException sqlre)
			{
				dbConnection.close();
				dbConnection = ConnectionManager.getFreshConnection();
				if(type.equalsIgnoreCase("Future"))
				{
					mSchedule = s.FetchFutureMatchSchedule(dbConnection);
				}
				else if(type.equalsIgnoreCase("Result"))
				{
					mSchedule = s.FetchResults(dbConnection);
				}
				else
				{
					mSchedule = s.FetchCurrentDaySchedule(dbConnection);
				}

			}
			catch(SQLNonTransientException sqlnte)
			{
				dbConnection.close();
				dbConnection = ConnectionManager.getFreshConnection();
				if(type.equalsIgnoreCase("Future"))
				{
					mSchedule = s.FetchFutureMatchSchedule(dbConnection);
				}
				else if(type.equalsIgnoreCase("Result"))
				{
					mSchedule = s.FetchResults(dbConnection);
				}
				else
				{
					mSchedule = s.FetchCurrentDaySchedule(dbConnection);
				}

			}
			catch(SQLException sqle)
			{
				connectionManager = ConnectionManager.getInstance();
				dbConnection = ConnectionManager.getFreshConnection();
				if(type.equalsIgnoreCase("Future"))
				{
					mSchedule = s.FetchFutureMatchSchedule(dbConnection);
				}
				else if(type.equalsIgnoreCase("Result"))
				{
					mSchedule = s.FetchResults(dbConnection);
				}
				else
				{
					mSchedule = s.FetchCurrentDaySchedule(dbConnection);
				}

				sqle.printStackTrace();
			}
		}
		catch(SQLException sqle)
		{
			dbConnection = ConnectionManager.getFreshConnection();
			sqle.printStackTrace();
		}
		catch(IOException ioe)
		{
			ioe.printStackTrace();
		}

		return mSchedule;
	}



	public static void saveUserPreferences(int matchNumber, String user_email_id, String preferences) {

		String key = matchNumber + "$$$$" + user_email_id.trim().toLowerCase();

		// Using the synchronous cache
		MemcacheController memCacheController = MemcacheController.getInstance();
		HashMap<String,Object>  syncCache = memCacheController.syncCache;
		User currentUserObj = new User();
		currentUserObj.setPreferences(preferences);
		currentUserObj.setUser_email_id(user_email_id);
		try{
		syncCache.put(key, currentUserObj); // populate cache
		}catch(Exception e){
			e.printStackTrace();
		}

		String[] preferencesArray = preferences.split(",");

		for (String individualPreference : preferencesArray) {
			key = matchNumber + "$$$$" + individualPreference.trim().toLowerCase();
			if (syncCache.containsKey(key)) {
				HashSet<String> usersForThisMatchAndAction = (HashSet<String>) syncCache.get(key);
				usersForThisMatchAndAction.add(user_email_id);
			} else {
				HashSet<String> userSet = new HashSet<String>();
				userSet.add(user_email_id);
				syncCache.put(key, userSet);
			}

		}

	}

	public String fetchUserPreferences(int matchNumber, String user_email_id) {

		String key = matchNumber + "$$$$" + user_email_id;

		// Using the synchronous cache
		MemcacheController memCacheController = MemcacheController.getInstance();
		HashMap<String,Object>  syncCache = memCacheController.syncCache;
		if (syncCache.containsKey(key)) {
			User desiredUser = (User) syncCache.get(key);
			return desiredUser.getPreferences();
		}

		return null;

	}

	public Set<String> fetchUsersForAction(int matchNumber, String preference) {

		String key = matchNumber + "$$$$" + preference;
		MemcacheController memCacheController = MemcacheController.getInstance();
		HashMap<String,Object> syncCache = memCacheController.syncCache;
		return (Set<String>) syncCache.get(key);

	}
}
