package edu.columbia.sportsleagueandroid.server;

import com.google.web.bindery.requestfactory.shared.Locator;


public class PlayerLocator extends Locator<Player, Void> {

	@Override
	public Player create(Class<? extends Player> clazz) {
		return new Player();
	}

	@Override
	public Player find(Class<? extends Player> clazz, Void id) {
		return create(clazz);
	}

	@Override
	public Class<Player> getDomainType() {
		return Player.class;
	}

	@Override
	public Void getId(Player domainObject) {
		return null;
	}

	@Override
	public Class<Void> getIdType() {
		return Void.class;
	}

	@Override
	public Object getVersion(Player domainObject) {
		return null;
	}

}
