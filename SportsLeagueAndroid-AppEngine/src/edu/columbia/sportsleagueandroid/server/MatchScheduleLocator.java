package edu.columbia.sportsleagueandroid.server;

import com.google.web.bindery.requestfactory.shared.Locator;


public class MatchScheduleLocator extends Locator<MatchSchedule, Void> {

	@Override
	public MatchSchedule create(Class<? extends MatchSchedule> clazz) {
		return new MatchSchedule();
	}

	@Override
	public MatchSchedule find(Class<? extends MatchSchedule> clazz, Void id) {
		return create(clazz);
	}

	@Override
	public Class<MatchSchedule> getDomainType() {
		return MatchSchedule.class;
	}

	@Override
	public Void getId(MatchSchedule domainObject) {
		return null;
	}

	@Override
	public Class<Void> getIdType() {
		return Void.class;
	}

	@Override
	public Object getVersion(MatchSchedule domainObject) {
		return null;
	}

}
