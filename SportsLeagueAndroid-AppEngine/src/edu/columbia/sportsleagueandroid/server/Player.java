package edu.columbia.sportsleagueandroid.server;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Player 
{
	@Id
	private String playerName;
	private String playerTeam;
	private int playerAge;
	private int matchesPlayed;
	private int runsScored;
	private int wicketsTaken;
	
	public Player()
	{}
	
	public String getPlayerName() 
	{
		return playerName;
	}
	
	public void setPlayerName(String playerName) 
	{
		this.playerName = playerName;
	}
	
	public String getPlayerTeam() 
	{
		return playerTeam;
	}
	
	public void setPlayerTeam(String playerTeam) 
	{
		this.playerTeam = playerTeam;
	}
	
	public int getPlayerAge() 
	{
		return playerAge;
	}
	
	public void setPlayerAge(int playerAge) 
	{
		this.playerAge = playerAge;
	}
	
	public int getMatchesPlayed() 
	{
		return matchesPlayed;
	}
	
	public void setMatchesPlayed(int matchesPlayed) 
	{
		this.matchesPlayed = matchesPlayed;
	}
	
	public int getRunsScored() 
	{
		return runsScored;
	}
	
	public void setRunsScored(int runsScored) 
	{
		this.runsScored = runsScored;
	}
	
	public int getWicketsTaken() 
	{
		return wicketsTaken;
	}
	
	public void setWicketsTaken(int wicketsTaken) 
	{
		this.wicketsTaken = wicketsTaken;
	}
	
}
