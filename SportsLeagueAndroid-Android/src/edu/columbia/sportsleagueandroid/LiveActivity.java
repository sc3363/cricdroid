package edu.columbia.sportsleagueandroid;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;

import com.google.web.bindery.requestfactory.shared.Receiver;
import com.google.web.bindery.requestfactory.shared.ServerFailure;

import edu.columbia.sportsleagueandroid.client.MyRequestFactory;
import edu.columbia.sportsleagueandroid.shared.MatchScheduleProxy;

import android.app.ListActivity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.AdapterView.OnItemClickListener;

public class LiveActivity extends ListActivity 
{
	Connection dbConnection;
	@Override
	public void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
	}
	
	private void setScreenContent()
	{
		setContentView(R.layout.live_layout);
		SharedPreferences prefs = Util.getSharedPreferences(this);
        final String accountName = prefs.getString(Util.ACCOUNT_NAME, "Nothing");
		
		final List<MatchScheduleProxy> list = new ArrayList<MatchScheduleProxy>();
		
		// Use an AsyncTask to avoid blocking the UI thread
		new AsyncTask<Void, Void, List<MatchScheduleProxy>>() 
		{
			@Override
			protected List<MatchScheduleProxy> doInBackground(Void... arg0) 
			{
				MyRequestFactory requestFactory = Util.getRequestFactory(getBaseContext(), MyRequestFactory.class);
				
				requestFactory.sportsLeagueAndroidRequest().queryMatchSchedules("Current").fire(new Receiver<List<MatchScheduleProxy>>() 
				{
					@Override
					public void onFailure(ServerFailure error)
					{
						String message = "Failure: "+ error.getMessage();
					}

					@Override
					public void onSuccess(List<MatchScheduleProxy> result) 
					{
						list.addAll(result);
					}
				});
				return list;
			}

			@Override
			protected void onPostExecute(List<MatchScheduleProxy> list) 
			{
				String[] currentMatches = new String[list.size()];
				
				int counter = 0;
				for (MatchScheduleProxy mProxy : list) 
				{
					StringBuilder sB = new StringBuilder();
					sB.append("Match# "+mProxy.getMatchNumber()+" | '");
					sB.append(mProxy.getTeamOne());
					sB.append(" vs ");
					sB.append(mProxy.getTeamTwo()+"' \n");
					sB.append(mProxy.getStadiumName()+", "+mProxy.getStadiumCity());
					currentMatches[counter++] = sB.toString();
				}

				ArrayAdapter<String> adapter = new ArrayAdapter<String>(getBaseContext(), R.layout.list_item, currentMatches);
		
				setListAdapter(adapter);
				adapter.notifyDataSetChanged();
				ListView lv = getListView();
				lv.setTextFilterEnabled(true);

				lv.setOnItemClickListener(new OnItemClickListener()
				{
					public void onItemClick(AdapterView<?> lv, View view,int position, long id)
					{
						Intent myIntent = new Intent(getParent(), ScoreCardActivity.class);
						myIntent.putExtra("extra", lv.getItemAtPosition(position).toString());
						MatchScoreActivityGroup parentActivity = (MatchScoreActivityGroup)getParent();
						parentActivity.startChildActivity("MatchScoreActivityGroup", myIntent);
					}
				});

			}
		}.execute();
	}

	@Override
	public void onResume() 
	{
	//	onDestroy();
		super.onResume();
		
		setScreenContent();
		setContentView(R.layout.live_layout);
	}
	
	 @Override
	 public void onDestroy()
	 {
	     super.onDestroy();
	 }
}