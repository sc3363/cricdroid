package edu.columbia.sportsleagueandroid.server;

import com.google.web.bindery.requestfactory.shared.Locator;


public class NewsLocator extends Locator<News, Void> {

	@Override
	public News create(Class<? extends News> clazz) {
		return new News();
	}

	@Override
	public News find(Class<? extends News> clazz, Void id) {
		return create(clazz);
	}

	@Override
	public Class<News> getDomainType() {
		return News.class;
	}

	@Override
	public Void getId(News domainObject) {
		return null;
	}

	@Override
	public Class<Void> getIdType() {
		return Void.class;
	}

	@Override
	public Object getVersion(News domainObject) {
		return null;
	}

}
