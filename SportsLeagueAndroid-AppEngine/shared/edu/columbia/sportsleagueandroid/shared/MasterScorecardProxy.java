package edu.columbia.sportsleagueandroid.shared;

import java.util.List;

import com.google.web.bindery.requestfactory.shared.ProxyForName;
import com.google.web.bindery.requestfactory.shared.ValueProxy;

@ProxyForName(value = "edu.columbia.sportsleagueandroid.server.MasterScorecard", locator = "edu.columbia.sportsleagueandroid.server.MasterScorecardLocator")
public interface MasterScorecardProxy extends ValueProxy 
{

	int getMatchNumber();

	void setMatchNumber(int matchNumber);

	String getTeamOne();

	void setTeamOne(String teamOne);

	String getTeamTwo();

	void setTeamTwo(String teamTwo);

	String getStadiumName();

	void setStadiumName(String stadiumName);

	String getStadiumCity();

	void setStadiumCity(String stadiumCity);

	int getScore();

	void setScore(int score);

	int getWickets();

	void setWickets(int wickets);

	double getOvers();

	void setOvers(double overs);

	List<BatsmanProxy> getBatsman();

	void setBatsman(List<BatsmanProxy> batsman);

	List<BowlerProxy> getBowler();

	void setBowler(List<BowlerProxy> bowler);

}
